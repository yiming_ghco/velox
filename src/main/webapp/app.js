// THIS FILE IS AUTO GENERATED. ANY CHANGES MADE TO IT MANUALLY WILL BE LOST. MODIFY ONE OF THE SOURCE .ts OR .js FILES
class App {
    constructor(env) {
        // const chooseSkin = () => {
        //     const skin = document.createElement('div');
        //     skin.innerHTML = `<span>Skin</span>
        //     <select class="skinName" onchange="document.getElementById('velox-skin').href=this.value">
        //     <option value='./veloxbase.css'>Default</option>
        //     <option value='./veloxcarbon.css'>Carbon</option>
        //     </select>`;
        //     return skin;
        // };
        this.env = env;
        const keyboardHelp = () => {
            const item = document.createElement('a');
            item.innerHTML = "Keyboard Help";
            item.onclick = () => {
                this.loadKeymap().then((layout) => {
                    const container = document.createElement("div");
                    document.body.appendChild(container);
                    const modal = new AralisModal(null, container, layout, "Shortcuts");
                    const promise = modal.showModal();
                    promise.then(() => {
                        container.remove();
                    });
                });
            };
            return item;
        };
        const showUserId = (env) => {
            const item = document.createElement('span');
            const icon = document.createElement("i");
            icon.classList.add("fa", "fa-user-circle");
            item.appendChild(icon);
            const user = document.createTextNode(env.user() || "");
            item.appendChild(user);
            item.classList.add("user");
            const userPopover = document.createElement("div");
            userPopover.classList.add("user-popover", "flex-row", "ai-center");
            const changePassword = document.createElement("button");
            changePassword.classList.add("btn", "btn-xs", "btn-primary", "changePassword");
            changePassword.innerHTML = "Change Password";
            userPopover.appendChild(changePassword);
            const instance = tippy(item, {
                content: userPopover,
                arrow: true,
                trigger: "click",
                interactive: true,
                animation: 'shift-away',
                appendTo: "parent",
                sticky: "true"
            });
            changePassword.onclick = () => {
                env.sendCommand({
                    name: "com.aralis.gooey.api.ScreenRequest",
                    value: { name: "com.velox.rms.api.RMSUserSettingScreen" }
                });
                instance.hide();
            };
            return item;
        };
        new DefaultLaunchPadFactory(env, {
            container: document.body,
            menus: {
                keyboardHelp: {
                    factory: keyboardHelp,
                    location: "left",
                    action: true
                },
                showUserId: {
                    factory: showUserId,
                    location: "right",
                    action: true
                }
            }
        });
    }
    start() {
        this.env.start();
        if (this.env.isMaster()) {
            this.login();
        }
    }
    login() {
        const loginHtml = document.createElement("object");
        loginHtml.type = "text/plain";
        loginHtml.data = "login.html";
        loginHtml.onload = function () {
            document.body.innerHTML = loginHtml.contentDocument.body.innerHTML;
            loginHtml.remove();
        };
        document.body.appendChild(loginHtml);
    }
    loadKeymap() {
        const promise = new Promise((resolve, reject) => {
            const request = document.createElement("object");
            request.type = "text/plain";
            request.data = "KeymapScreen.html";
            request.onload = function () {
                resolve(request.contentDocument.body.innerHTML);
                request.remove();
            };
            document.body.appendChild(request);
        });
        return promise;
    }
}
window.onload = function () {
    const app = new App(environment);
    app.start();
};
//# sourceMappingURL=app.js.map