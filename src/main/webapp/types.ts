declare interface LaunchPadFactory {
    addMenuItem(name: string, item: MenuItem): void;
}

declare interface MenuItem {
    factory: (env?: Environment) => HTMLElement;
    location: "left" | "right";
    action: boolean;
}

declare interface LaunchPadOptions {
    container: HTMLElement;

    menus: { [index: string]: MenuItem | boolean }
}

declare class DefaultLaunchPadFactory implements LaunchPadFactory {
    constructor(environment: Environment, options: LaunchPadOptions)

    addMenuItem(name: string, item: MenuItem): void;
}

declare type AppMode = "SPA" | "MPA" ;

declare interface Environment {
    isMaster(): boolean;

    appMode(): AppMode;

    user(): string;

    start(): void;

    sendCommand(command: any): void;
}

