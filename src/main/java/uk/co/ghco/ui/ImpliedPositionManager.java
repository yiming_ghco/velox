package uk.co.ghco.ui;

import com.aralis.math.MathUtil;

public class ImpliedPositionManager {
    public static double calculateNotional(double fx, double price, double qty) {
        double notional = qty * price;
        double fxAdjustedNotional;
        if (!MathUtil.isZeroOrNan(fx)) {
            fxAdjustedNotional = notional/fx;
        } else {
            fxAdjustedNotional = notional;
        }
        return fxAdjustedNotional;
    }
}
