package uk.co.ghco.ui;

import com.aralis.config.EntityConfigProvider;
import com.aralis.df.cache.CachePublisherTrackingFactory;
import com.aralis.df.cache.CacheTable;
import com.aralis.df.cache.state.DataContextAccessor;
import com.aralis.df.cache.viewserver.CompositeKeyConverter;
import com.aralis.df.extractor.ConverterBoolean;
import com.aralis.logging.Logger;
import com.aralis.logging.LoggerFactory;
import com.aralis.tools.ScreenConfigurator;
import com.aralis.tools.configuration.EntityNamespaceProvider;
import com.aralis.tools.configuration.ui.UIConfigNamespace;
import com.aralis.tools.util.OKCancelHelper;
import com.aralis.vm.ClientNotifier;
import com.aralis.vm.SessionState;
import com.aralis.vm.WorkspaceScreenProvider;
import com.aralis.vm.workspace.DataLink;
import com.aralis.vm.workspace.ScreenLinkHelper;
import uk.co.ghco.api.*;
import uk.co.ghco.util.ConditionalFormatHelper;
import java.util.Arrays;

public class TradeViewerScreenProvider extends WorkspaceScreenProvider<TradeViewerScreen> {
    private static final Logger s_log = LoggerFactory.getLogger();

    public TradeViewerScreenProvider(String caption, String group, String icon) {
        super(TradeViewerScreen.class, caption, group, icon);
    }

    @Override
    public TradeViewerScreen createScreen(final SessionState sessionState, final ClientNotifier clientNotifier) {
        DataContextAccessor dc = sessionState.getExisting(DataContextAccessor.class);
        CachePublisherTrackingFactory registry = sessionState.getExisting(CachePublisherTrackingFactory.class);
        EntityConfigProvider<UIConfigNamespace> ecp = EntityNamespaceProvider.getUiConfig(sessionState);

        TradeViewerScreen screen = new TradeViewerScreen(sessionState.getThreadingContext(),
          ScreenConfigurator.create(ecp),
          dc.getTable(TradeJoin.class));
        screen.title(caption());

        ConditionalFormatHelper.ViewConfigUpdateListener listener =
          new ConditionalFormatHelper.ViewConfigUpdateListener(TradeJoinView.ViewDefinition);
        listener.accept(screen.m_trades, screen.m_trades.viewConfig());

        // The following code adds support to allow incoming link from Position viewer to Order
        // and links the two with matching symbol. The linking key is set in the onLink function
        screen.onRequestLink(onLink(screen, dc.getTable(TradeJoin.class)),
          onUnlink(screen, dc.getTable(TradeJoin.class)));

        OKCancelHelper okCancelHelper = new OKCancelHelper(clientNotifier, ecp, screen);
        return screen;
    }

    private final ConverterBoolean<DataLink> onLink(
      final TradeViewerScreen screen, final CacheTable<TradeJoin, Object> data) {

        return link -> {
            s_log.info(
              "Link grids. source type: " + link.source().getClazz().getSimpleName() + " target type: " + link.target()
                .getClazz()
                .getSimpleName());
            /*-
             * The link request contains the source CacheTable, the target CacheTable
             * The linking logic has to enumerate all linkable source/target CacheTable pairs
             * and specify the proper key extractors to establish the linkage
             */
            if (link.target() == screen.m_trades) {
                // if link target is the exec grid
                final Class<?> from = link.source().getClazz();
                if (from == PositionJoin.class) {
                    // if the source object type is OrderStaticJoin, will link source and target by
                    // matching the order id on the order with the order id on the execution
                    // The base population to find all the linked object is from the "data" CacheTable
                    ScreenLinkHelper.linkGrids(link.source(),
                      CompositeKeyConverter.create(Arrays.asList(PositionJoinExtractor.psj_position_accountIdExtractor,
                        PositionJoinExtractor.psj_position_portfolioNameExtractor,
                        PositionJoinExtractor.psj_position_listingIdExtractor,
                        PositionJoinExtractor.psj_position_currencyExtractor)),
                      link.target(),
                      data,
                      CompositeKeyConverter.create(Arrays.asList(TradeJoinExtractor.trade_accountIdExtractor,
                        TradeJoinExtractor.trade_portfolioNameExtractor,
                        TradeJoinExtractor.trade_listingIdExtractor,
                        TradeJoinExtractor.trade_tradeCcyExtractor)));
                    return true;
                }
            }
            return false;
        };
    }

    private final ConverterBoolean<DataLink> onUnlink(
      final TradeViewerScreen screen, final CacheTable<TradeJoin, Object> data) {
        return link -> {
            if (link.target() == screen.m_trades) {
                ScreenLinkHelper.unlinkGrid(link.target(), data);
                return true;
            }
            return false;
        };
    }

}
