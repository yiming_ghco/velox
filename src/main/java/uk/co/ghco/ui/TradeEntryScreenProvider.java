package uk.co.ghco.ui;

import com.aralis.config.EntityConfigProvider;
import com.aralis.df.cache.CachePublisherTrackingFactory;
import com.aralis.df.cache.state.DataContextAccessor;
import com.aralis.logging.Logger;
import com.aralis.logging.LoggerFactory;
import com.aralis.strings.StringUtil;
import com.aralis.tools.ScreenConfigurator;
import com.aralis.tools.configuration.EntityNamespaceProvider;
import com.aralis.tools.configuration.ui.UIConfigNamespace;
import com.aralis.tools.util.OKCancelHelper;
import com.aralis.vm.ClientNotifier;
import com.aralis.vm.SessionState;
import com.aralis.vm.WorkspaceScreenProvider;
import uk.co.ghco.api.*;
import uk.co.ghco.api.ghcoengine.client.GHCOEngineSnapshotClient;
import java.time.Instant;
import java.time.LocalDate;

public class TradeEntryScreenProvider extends WorkspaceScreenProvider<TradeEntryScreen> {
    private static final Logger s_log = LoggerFactory.getLogger();
    private final GHCOEngineSnapshotClient m_snapshotClient;

    public TradeEntryScreenProvider(String caption, String group, String icon, final GHCOEngineSnapshotClient client) {
        super(TradeEntryScreen.class, caption, group, icon);
        m_snapshotClient = client;
    }

    @Override
    public TradeEntryScreen createScreen(final SessionState sessionState, final ClientNotifier clientNotifier) {
        DataContextAccessor dc = sessionState.getExisting(DataContextAccessor.class);
        CachePublisherTrackingFactory registry = sessionState.getExisting(CachePublisherTrackingFactory.class);
        EntityConfigProvider<UIConfigNamespace> ecp = EntityNamespaceProvider.getUiConfig(sessionState);

        TradeEntryScreen screen =
          new TradeEntryScreen(sessionState.getThreadingContext(), ScreenConfigurator.create(ecp));
        screen.title(caption());

        OKCancelHelper okCancelHelper = new OKCancelHelper(clientNotifier, ecp, screen);
        screen.m_clear.setListener(t -> {
            screen.m_portfolioName.setValue(null);
            screen.m_account.setValue(null);
            screen.m_symbol.setValue(null);
            screen.m_tradeCcy.setValue(null);
            screen.m_tradeQty.setValue(null);
            screen.m_tradePrice.setValue(null);
            screen.m_side.setValue(null);
        });
        screen.m_create.setListener(t -> {
            if (StringUtil.isEmpty(screen.m_portfolioName.getValue())) {
                okCancelHelper.show("Please set portfolio name");
                return;
            }
            if (StringUtil.isEmpty(screen.m_account.getValue())) {
                okCancelHelper.show("Please set account");
                return;
            }
            if (StringUtil.isEmpty(screen.m_symbol.getValue())) {
                okCancelHelper.show("Please set symbol");
                return;
            }
            if (screen.m_side.getValue() == null) {
                okCancelHelper.show("Please set side");
                return;
            }
            if (StringUtil.isEmpty(screen.m_tradeCcy.getValue())) {
                okCancelHelper.show("Please set currency");
                return;
            }
            if (screen.m_tradeQty.getValue() == null) {
                okCancelHelper.show("Please set trade qty");
                return;
            }
            if (screen.m_tradePrice.getValue() == null) {
                okCancelHelper.show("Please set trade price");
                return;
            }
            m_snapshotClient.sendCommand(CreateTradeBuilder.newBuilder()
              .portfolioName(screen.m_portfolioName.getValue())
              .accountId(screen.m_account.getValue())
              .tradeSource(TradeSource.MANUAL)
              .tradeListingId(screen.m_symbol.getValue())
              .side(screen.m_side.getValue())
              .tradeDate(LocalDate.now())
              .tradeCcy(screen.m_tradeCcy.getValue())
              .tradePrice(screen.m_tradePrice.getValue())
              .tradeQty(screen.m_tradeQty.getValue())
              .execTime(Instant.now())
              .get());
        });
        return screen;
    }
}
