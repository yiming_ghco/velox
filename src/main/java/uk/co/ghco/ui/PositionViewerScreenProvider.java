package uk.co.ghco.ui;

import com.aralis.config.EntityConfigProvider;
import com.aralis.df.cache.CachePublisherTrackingFactory;
import com.aralis.df.cache.state.DataContextAccessor;
import com.aralis.logging.Logger;
import com.aralis.logging.LoggerFactory;
import com.aralis.tools.ScreenConfigurator;
import com.aralis.tools.configuration.EntityNamespaceProvider;
import com.aralis.tools.configuration.ui.UIConfigNamespace;
import com.aralis.vm.ClientNotifier;
import com.aralis.vm.SessionState;
import com.aralis.vm.WorkspaceScreenProvider;
import uk.co.ghco.api.PositionJoin;
import uk.co.ghco.api.PositionViewerScreen;

public class PositionViewerScreenProvider extends WorkspaceScreenProvider<PositionViewerScreen> {
    private static final Logger s_log = LoggerFactory.getLogger();

    public PositionViewerScreenProvider(String caption, String group, String icon) {
        super(PositionViewerScreen.class, caption, group, icon);
    }

    @Override
    public PositionViewerScreen createScreen(final SessionState sessionState, final ClientNotifier clientNotifier) {
        DataContextAccessor dc = sessionState.getExisting(DataContextAccessor.class);
        CachePublisherTrackingFactory registry = sessionState.getExisting(CachePublisherTrackingFactory.class);
        EntityConfigProvider<UIConfigNamespace> ecp = EntityNamespaceProvider.getUiConfig(sessionState);

        PositionViewerScreen screen = new PositionViewerScreen(sessionState.getThreadingContext(),
          ScreenConfigurator.create(ecp),
          dc.getTable(PositionJoin.class));
        screen.title(caption());

        return screen;
    }
}
