package uk.co.ghco.ui;

import com.aralis.config.EntityConfigProvider;
import com.aralis.df.cache.CachePublisher;
import com.aralis.df.cache.CachePublisherTrackingFactory;
import com.aralis.df.cache.CacheTable;
import com.aralis.df.cache.index.Indexer;
import com.aralis.df.cache.index.Indexer2;
import com.aralis.df.cache.state.DataContextAccessor;
import com.aralis.strings.StringUtil;
import com.aralis.tools.ScreenConfigurator;
import com.aralis.tools.configuration.EntityNamespaceProvider;
import com.aralis.tools.configuration.ui.UIConfigNamespace;
import com.aralis.tools.util.OKCancelHelper;
import com.aralis.vm.ClientNotifier;
import com.aralis.vm.SessionState;
import com.aralis.vm.WorkspaceScreenProvider;
import com.velox.data.injector.UltumusDataInjector;
import com.velox.data.reference.api.*;
import com.velox.data.util.DateHelper;
import com.velox.data.util.product.BasketUtil;
import uk.co.ghco.api.Product;
import uk.co.ghco.api.*;
import uk.co.ghco.application.GHCOApplication;
import java.time.LocalDate;
import java.util.Collection;

public class ImpliedPositionViewerScreenProvider extends WorkspaceScreenProvider<ImpliedPositionViewerScreen> {
    private final GHCOApplication m_application;

    public ImpliedPositionViewerScreenProvider(String caption, String group, String icon, GHCOApplication app) {
        super(ImpliedPositionViewerScreen.class, caption, group, icon);
        m_application = app;
    }

    @Override
    public ImpliedPositionViewerScreen createScreen(SessionState sessionState, ClientNotifier notifier) {
        DataContextAccessor dc = sessionState.getExisting(DataContextAccessor.class);
        // get hold of registry to create publishers
        CachePublisherTrackingFactory registry = sessionState.getExisting(CachePublisherTrackingFactory.class);
        EntityConfigProvider<UIConfigNamespace> ecp = EntityNamespaceProvider.getUiConfig(sessionState);

        // create transient table
        CachePublisher<PositionJoin, Object> impliedPositionJoins =
          registry.create(PositionJoinTableDescriptor.Descriptor, "implied");

        ImpliedPositionViewerScreen screen = new ImpliedPositionViewerScreen(sessionState.getThreadingContext(),
          ScreenConfigurator.create(ecp),
          dc.getTable(PositionJoin.class),
          impliedPositionJoins.getTable());
        screen.title(caption());

        CachePublisher<Position, Object> impliedPositions =
          registry.create(PositionTableDescriptor.Descriptor, "rawImpliedPositions");

        // wire up the join
//        PositionJoinJoinBuilderFactory.create(impliedPositionJoins,
//          screen.dispatcher(),
//          screen.cancellationManager(),
//          impliedPositions.getTable(),
//          dc.getTable(Product.class),
//          dc.getTable(MarketData.class),
//          dc.getTable(MarketData.class));

        LocalDate today = DateHelper.getToday();
        CacheTable<SubBasketProductAttribute, Object> subBasketProductTable =
          dc.getTable(SubBasketProductAttribute.class);
        CacheTable<BasketProduct, Object> basketProductTable = dc.getTable(BasketProduct.class);
        Indexer2<Object, String, LocalDate> index =
          basketProductTable.getMultiIndexer(BasketProductExtractor.symbol_tickerExtractor,
            BasketProductExtractor.validDateExtractor);

        UltumusDataInjector dataInjector = m_application.getUltumusDataInjector();

        OKCancelHelper okCancelHelper = new OKCancelHelper(notifier, ecp, screen);

        BasketUtil bu = new BasketUtil(subBasketProductTable, m_application.getBasketConstituentsLoader());
        StringBuffer errorMsg = new StringBuffer();

        screen.m_positions.addSelectionEventListener((g, pjs) -> {
//            impliedPositions.clear();
//
//            for (PositionJoin p : pjs) {
//                BasketProduct bp = basketProductTable.value(index.keys(p.position().productId(), today));
//                if (bp == null) {
//                    UltumusSymbolMap map = mapTable.value(mapIndex.keys(p.position().productId()));
//                    if (map != null) {
//                        bp = dataInjector.downloadAndLoadETF(map.ultumusId(), today);
//                    } else {
//                        errorMsg.append("Unable to find ultumus id for ETF symbol " + p.position().productId());
//                        continue;
//                    }
//                }
//                SubBasketProductAttribute sbpa = bu.subBasket(bp);
//                Collection<BasketConstituent> cs = bu.constituents(sbpa);
//                for (BasketConstituent c : cs) {
//                    double scalingFactor = c.sharesInUnit() / bp.sharesPerUnit();
//                    String ccy = c.currency() != null ? c.currency().toString() : "";
//                    String cpid = BasketConstituentExtractor.symbol_bloombergExtractor.get(c);
//                    Position constituentImpliedPosition = PositionBuilder.newBuilder()
//                      .accountId(p.position().accountId())
//                      .portfolioName(p.position().portfolioName())
//                      .currency(ccy)
//                      .productId(cpid)
//                      .boughtQty(p.position().boughtQty() * scalingFactor)
//                      .soldQty(p.position().soldQty() * scalingFactor)
//                      .get();
//                    impliedPositions.publish(constituentImpliedPosition);
//                }
//            }
//            if (!StringUtil.isEmpty(errorMsg.toString())) {
//                okCancelHelper.show(errorMsg.toString());
//            }
        });
        return screen;
    }
}
