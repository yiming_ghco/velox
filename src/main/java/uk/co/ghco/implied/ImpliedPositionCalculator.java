package uk.co.ghco.implied;

import com.aralis.application.ApplicationContext;
import com.aralis.df.cache.CachePublisher;
import com.aralis.df.cache.CacheTable;
import com.aralis.df.cache.ClearingBulkListener;
import com.aralis.df.cache.helpers.ConflatingListener;
import com.aralis.df.cache.index.Indexer4;
import com.aralis.df.cache.state.DataContextAccessor;
import com.aralis.lists.ChangePair;
import com.aralis.threads.SimpleCancellationManager;
import com.aralis.threads.SimpleThreadingContext;
import com.aralis.threads.ThreadingContext;
import com.velox.data.injector.BasketConstituentsLoader;
import com.velox.data.reference.api.BasketConstituent;
import com.velox.data.reference.api.BasketConstituentExtractor;
import com.velox.data.reference.api.BasketProduct;
import com.velox.data.reference.api.SubBasketProductAttribute;
import com.velox.data.util.product.BasketUtil;
import uk.co.ghco.api.*;
import uk.co.ghco.application.GHCOApplication;
import uk.co.ghco.application.GHCOComponent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.Collections.emptyList;

public class ImpliedPositionCalculator implements ClearingBulkListener<PositionBasketJoin> {
    private final CachePublisher<ImpliedPositionRoot, Object> m_impliedPositionsSink;
    private final BasketUtil m_basketUtil;
    private final Indexer4<Object, String, String, String, String> m_positionIndexer;
    private final CacheTable<PositionBasketJoin, Object> m_allPositions;
    private final ThreadingContext m_tc;

    public ImpliedPositionCalculator(
      ThreadingContext tc,
      CacheTable<PositionBasketJoin, Object> allPositions,
      CacheTable<SubBasketProductAttribute, Object> subBasketProductTable,
      CachePublisher<ImpliedPositionRoot, Object> impliedPositionsSink,
      BasketConstituentsLoader bcl) {
        m_impliedPositionsSink = impliedPositionsSink;
        m_positionIndexer =
          m_impliedPositionsSink.getTable().getMultiIndexer(ImpliedPositionRootExtractor.rootListingIdExtractor,
            ImpliedPositionRootExtractor.accountIdExtractor,
            ImpliedPositionRootExtractor.portfolioNameExtractor,
            ImpliedPositionRootExtractor.currencyExtractor);
        m_basketUtil = new BasketUtil(subBasketProductTable, bcl);
        m_allPositions = allPositions;
        m_tc = tc;
    }

    public void run() {
        SimpleCancellationManager cm = new SimpleCancellationManager();
        ConflatingListener<PositionBasketJoin, Object> conflater =
          new ConflatingListener<>(this, m_allPositions.keyConverter(), m_tc);
        m_allPositions.subscribe(conflater, cm);
    }

    @Override
    public void clear() {
        m_impliedPositionsSink.clear();
    }

    @Override
    public void batch(
      Collection<PositionBasketJoin> ps,
      Collection<ChangePair<PositionBasketJoin>> cs,
      Collection<PositionBasketJoin> rs) {
        for (PositionBasketJoin pbj : ps) {
            recreateImplied(null, pbj);
        }

        for (ChangePair<PositionBasketJoin> change : cs) {
            recreateImplied(change.getOld(), change.getNew());
        }

        for (PositionBasketJoin r : rs) {
            clearExisting(r);
        }
    }

    private void recreateImplied(PositionBasketJoin oldP, PositionBasketJoin newP) {
        if (oldP == null || oldP.basketProduct() != newP.basketProduct()) {
            clearExisting(oldP);
            createImplied(newP);
        }
    }

    private void createImplied(PositionBasketJoin pbj) {
        BasketProduct bp = pbj.basketProduct();
        SubBasketProductAttribute sbpa = m_basketUtil.subBasket(bp);
        Collection<BasketConstituent> cs = m_basketUtil.constituents(sbpa);
        List<ImpliedPositionRoot> roots = new ArrayList<>(cs.size());
        for (BasketConstituent c : cs) {
            Position position = pbj.psj().position();
            roots.add(ImpliedPositionRootBuilder.newBuilder()
              .accountId(position.accountId())
              .portfolioName(position.portfolioName())
              .currency(position.currency())
              .rootListingId(position.listingId())
              .productId(c.productId())
              .marketDataId(BasketConstituentExtractor.symbol_bloombergExtractor.get(c))
              .basketConstituent(c)
              .get());
        }
        m_impliedPositionsSink.publish(roots);
    }

    private void clearExisting(PositionBasketJoin pbj) {
        if (pbj != null && pbj.basketProduct() != null) {
            Position position = pbj.psj().position();
            Collection<Object> toRemove = m_positionIndexer.keys(position.listingId(),
              position.accountId(),
              position.portfolioName(),
              position.currency());
            m_impliedPositionsSink.eventWithKeys(emptyList(), toRemove);
        }
    }

    public static ImpliedPositionCalculator create(ApplicationContext<GHCOComponent> ctx) {
        GHCOApplication application = ctx.get(GHCOComponent.GHCO);
        DataContextAccessor dc = ctx.get(GHCOComponent.SharedDataContext);
        return new ImpliedPositionCalculator(SimpleThreadingContext.dedicatedTC("ImpliedPositionCalculator").get(),
          dc.getTable(PositionBasketJoin.class),
          dc.getTable(SubBasketProductAttribute.class),
          dc.getPublisher(ImpliedPositionRoot.class),
          application.getBasketConstituentsLoader());
    }
}
