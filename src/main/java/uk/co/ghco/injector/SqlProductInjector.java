package uk.co.ghco.injector;

import com.aralis.application.ApplicationContext;
import com.aralis.df.cache.CachePublisher;
import com.aralis.df.cache.index.Indexer;
import com.aralis.df.cache.state.DataContextAccessor;
import uk.co.ghco.api.*;
import uk.co.ghco.api.ghcoengine.client.GHCOEngineSnapshotClient;
import uk.co.ghco.application.GHCOComponent;

import java.sql.*;
import java.time.LocalDate;
import java.util.Collection;

import static uk.co.ghco.injector.SqlPositionInjector.*;

public class SqlProductInjector {
    private final CachePublisher<MarketData, Object> m_marketDataSink;
    private final Indexer<Object, String> m_productIndexer;
    private final GHCOEngineSnapshotClient m_snapshotClient;
    CachePublisher<Product, Object> m_productSink;


    public SqlProductInjector(DataContextAccessor dc, GHCOEngineSnapshotClient snapshotClient) {
        m_productSink = dc.getPublisher(Product.class);
        m_productIndexer = m_productSink.getTable().getIndexer(ProductExtractor.productIdExtractor);
        m_marketDataSink = dc.getPublisher(MarketData.class);
        m_snapshotClient = snapshotClient;
    }

    public static SqlProductInjector create(ApplicationContext<GHCOComponent> ctx) {
        DataContextAccessor dc = ctx.get(GHCOComponent.SharedDataContext);
        GHCOEngineSnapshotClient snapshotClient = ctx.get(GHCOComponent.GHCOSnapshotClient);
        return new SqlProductInjector(dc, snapshotClient);
    }

    public void run() throws ClassNotFoundException, SQLException {
        Class.forName(s_dbClass);
        final Connection conn = DriverManager.getConnection(s_jdbcUrl, s_user, s_password);
        Statement stmt = conn.createStatement();
        String sql = "select * from GHCOpositions.dbo.vwVeloxProducts";
        ResultSet rs = stmt.executeQuery(sql);
        while (rs.next()) {
            String productId = rs.getString("ProductID");
//            Collection<Product> products = m_productSink.getTable().values(m_productIndexer.keys(productId));
//            if (products.isEmpty()) {
            m_snapshotClient.sendCommand(ProductBuilder.newBuilder()
                    .productId(productId)
                    .bbgCode(rs.getString("BBGCode"))
                    .multiplier(rs.getDouble("Multiplier"))
                    .currency(rs.getString("Currency"))
                    .instrumentType(rs.getString("InstrumentType"))
                    .basketSize(rs.getDouble("BasketSize"))
                    .isin(rs.getString("ISIN"))
                    .ultumusId(rs.getString("UltumusID"))
                    .orcId(rs.getString("MarketDataCode"))
                    .get());
//            Product product = ProductBuilder.newBuilder()
//                    .productId(productId)
//                    .multiplier(rs.getDouble("Multiplier"))
//                    .currency(rs.getString("Currency")).get();
//            m_productSink.publish(product);
//            MarketData marketData = MarketDataBuilder.newBuilder().marketDataId(productId).lastPrice(rs.getDouble("Price")).get();
//            m_marketDataSink.publish(marketData);
//            }
        }

        String constituentSql = "select * from GHCOpositions.dbo.vwVeloxListings ";
        ResultSet rsL = stmt.executeQuery(constituentSql);
        while (rsL.next()) {
            String bbg_code = rsL.getString("BBGCode");
            m_snapshotClient.sendCommand(ListingBuilder.newBuilder()
                    .productId(rsL.getString("ProductID"))
                    .listingId(bbg_code)
                    .currency(rsL.getString("Currency"))
                    .orcId(rsL.getString("MarketDataCode"))
                    .currenexId(rsL.getString("CurrenexID"))
                    .exchange(rsL.getString("exchangeCode"))
                    .safeKeeping(rsL.getString("Safekeeping"))
                    .get());

//            Product product = ProductBuilder.newBuilder()
//                    .productId(bbg_code)
//                    .currency(rsC.getString("CurrencyRisk"))
//                    .country(rsC.getString("CountryRisk"))
//                    .get();
//            m_productSink.publish(product);
            MarketData marketData = MarketDataBuilder.newBuilder()
                    .marketDataId(rsL.getString("MarketDataCode"))
                    .lastPrice(rsL.getDouble("Price"))
                    .get();
            m_marketDataSink.publish(marketData);
        }

        String constituenttSql = "select * from GHCOpositions.dbo.vwVeloxConstituents ";
        ResultSet rsC = stmt.executeQuery(constituenttSql);
        while (rsC.next()) {
            String bbg_code = rsC.getString("BBGCode");
            m_snapshotClient.sendCommand(ProductBuilder.newBuilder()
                    .productId(bbg_code)
                    .bbgCode(bbg_code)
                    .currency(rsC.getString("CurrencyRisk"))
                    .country(rsC.getString("CountryRisk"))
                    .sector(rsC.getString("SectorRisk"))
                    .orcId(rsC.getString("MarketDataCode"))
                    .instrumentType("STOCK")
                    .get());

//            Product product = ProductBuilder.newBuilder()
//                    .productId(bbg_code)
//                    .currency(rsC.getString("CurrencyRisk"))
//                    .country(rsC.getString("CountryRisk"))
//                    .get();
//            m_productSink.publish(product);
            MarketData marketData = MarketDataBuilder.newBuilder()
                    .marketDataId(rsC.getString("MarketDataCode"))
                    .lastPrice(rsC.getDouble("Price"))
                    .get();
            m_marketDataSink.publish(marketData);
        }
        String fxQuery = "SELECT CURRENCY_CODE, 1/PRICE AS PRICE FROM GHPnL.dbo.tClosingMarks WHERE PRODUCT_GROUP_CODE = 'FX' and PRICE_DATE = CONVERT(DATE, GHCOPositions.dbo.getTMinus1Workday(getDate())) and PRICE_SOURCE = 'Sova'";
        ResultSet rsF = stmt.executeQuery(fxQuery);
        while (rsF.next()) {
            MarketData marketData = MarketDataBuilder.newBuilder()
                    .marketDataId(rsF.getString("CURRENCY_CODE"))
                    .lastPrice(rsF.getDouble("PRICE"))
                    .get();
            m_marketDataSink.publish(marketData);
        }
    }
}
