package uk.co.ghco.injector;

import com.aralis.application.ApplicationContext;
import uk.co.ghco.api.*;
import uk.co.ghco.api.ghcoengine.client.GHCOEngineSnapshotClient;
import uk.co.ghco.application.GHCOComponent;

import java.sql.*;
import java.time.LocalDate;

public class SODPositionsInjector {
    final static String s_dbClass = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    final static String s_jdbcUrl = "jdbc:sqlserver://etf-sql:1433;database=GHCOpositions";
    final static String s_user = "sa";
    final static String s_password="J#rd#n10";
    private final GHCOEngineSnapshotClient m_snapshotClient;

    public SODPositionsInjector(GHCOEngineSnapshotClient snapshotClient) {
        m_snapshotClient = snapshotClient;
    }

    public static SODPositionsInjector create(ApplicationContext<GHCOComponent> ctx) throws SQLException, ClassNotFoundException {
        GHCOEngineSnapshotClient snapshotClient = ctx.get(GHCOComponent.GHCOSnapshotClient);
        SODPositionsInjector i = new SODPositionsInjector(snapshotClient);
        //i.run();
        return i;
    }

    public void run() throws SQLException, ClassNotFoundException {
        Class.forName(s_dbClass);

        final Connection conn = DriverManager.getConnection(s_jdbcUrl, s_user, s_password);
        Statement stmt = conn.createStatement();
        String sql = "SELECT * FROM Trading.risk.vwOpeningPositions";
        ResultSet rs = stmt.executeQuery(sql);
        while (rs.next()) {
            m_snapshotClient.sendCommand(CreatePositionSnapBuilder.newBuilder()
                    .positionSnapType(PositionSnapType.SOD)
                    .accountId(rs.getString("TradeGroup"))
                    .currency(rs.getString("Currency"))
                    .listingId(rs.getString("Identifier"))
                    .portfolioName(rs.getString("Portfolio"))
                    .costBasis(rs.getDouble("Price"))
                    .quantity(rs.getDouble("Position"))
                    .snapDate(LocalDate.now())
                    .get());
        }
    }
}
