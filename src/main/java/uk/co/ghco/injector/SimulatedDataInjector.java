package uk.co.ghco.injector;

import com.aralis.application.ApplicationContext;
import com.aralis.df.cache.CachePublisher;
import com.aralis.df.cache.state.DataContextAccessor;
import com.aralis.df.extractor.TableDescriptor;
import com.aralis.lists.Pair;
import com.aralis.tools.util.CSVToObjHelper;
import uk.co.ghco.api.*;
import uk.co.ghco.api.ghcoengine.client.GHCOEngineSnapshotClient;
import uk.co.ghco.application.GHCOComponent;
import uk.co.ghco.util.FileHelper;
import java.nio.charset.Charset;
import java.util.List;

/**
 * This is a simulated data injector. Dropcopy would be done following similar patterns
 */
public class SimulatedDataInjector {
    private final CachePublisher<Product, Object> m_productSink;
    private final GHCOEngineSnapshotClient m_stateEngineClient;

    public SimulatedDataInjector(
      final GHCOEngineSnapshotClient stateEngineClient,
      final CachePublisher<Product, Object> products) {
        m_stateEngineClient = stateEngineClient;
        m_productSink = products;
    }

    public void start() {
        ProductBuilder pb = ProductBuilder.newBuilder().productType(ProductType.EQUITY_COMMON);

        String products;
        products = FileHelper.readFile("/product.csv", Charset.defaultCharset(), this.getClass().getClassLoader());
        Pair<TableDescriptor, List<?>> productList = (Pair<TableDescriptor, List<?>>) CSVToObjHelper.convert(
          GHCOSchemaDescriptor.SchemaDescriptor,
          "Product",
          products);

        // Example code for CSV parser
        //        CSVWithMetaDataParser parser = new CSVWithMetaDataParser(CSVWithMetaDataParser.Delimiter.COMMA, true);
        //        ParseResult result = parser.apply(products);
        //        for (Map<String, String> row : result.rows()) {
        //            if (StringUtil.isEmpty(row.get("currency"))) {
        //                continue;
        //            }
        //            m_productSink.publish(ProductBuilder.newBuilder().symbol(row.get("symbol")).name(row.get
        //            ("name")).get());
        //        }

        m_productSink.publish((List<Product>) productList.getSecond());
    }

    public static SimulatedDataInjector create(final ApplicationContext<GHCOComponent> applicationContext) {
        DataContextAccessor da = applicationContext.get(GHCOComponent.SharedDataContext);
        CachePublisher<Product, Object> products = da.getPublisher(Product.class);
        GHCOEngineSnapshotClient client = applicationContext.get(GHCOComponent.GHCOSnapshotClient);
        return new SimulatedDataInjector(client, products);
    }

}
