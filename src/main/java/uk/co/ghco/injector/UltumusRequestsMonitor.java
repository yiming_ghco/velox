package uk.co.ghco.injector;

import com.aralis.application.ApplicationContext;
import com.aralis.df.cache.BulkListener;
import com.aralis.df.cache.CacheTable;
import com.aralis.df.cache.helpers.ImmutableFilterListener;
import com.aralis.df.cache.index.KeysActivator;
import com.aralis.df.cache.state.DataContextAccessor;
import com.aralis.requests.RequestsMonitor;
import com.aralis.threads.SimpleCancellationManager;
import com.aralis.threads.SimpleThreadingContext;
import com.aralis.threads.ThreadingContext;
import com.velox.data.injector.UltumusDataInjector;
import com.velox.data.util.DateHelper;
import uk.co.ghco.api.PositionStaticJoin;
import uk.co.ghco.api.PositionStaticJoinExtractor;
import uk.co.ghco.api.ProductType;
import uk.co.ghco.application.GHCOComponent;
import java.util.Set;

public class UltumusRequestsMonitor {
    private final UltumusDataInjector m_uji;
    private final SimpleCancellationManager m_cm;
    private final CacheTable<PositionStaticJoin, Object> m_allPositions;

    public UltumusRequestsMonitor(
      UltumusDataInjector uji, ThreadingContext tc, CacheTable<PositionStaticJoin, Object> allPositions) {
        m_cm = new SimpleCancellationManager();
        m_allPositions = allPositions;
        m_uji = uji;
        RequestsMonitor<String> indexMonitor = RequestsMonitor.create(m_indexSubscriber, tc);
        indexMonitor.monitor(this::subscribeToIndexPositions,
          PositionStaticJoinExtractor.listingProduct_product_ultumusIdExtractor,
          m_cm);

        RequestsMonitor<String> etfMonitor = RequestsMonitor.create(m_etfSubscriber, tc);
        etfMonitor.monitor(this::subscribeToETFPositions,
          PositionStaticJoinExtractor.listingProduct_product_ultumusIdExtractor,
          m_cm);
    }

    private void subscribeToETFPositions(BulkListener<PositionStaticJoin> bl) {
        m_allPositions.subscribe(new ImmutableFilterListener<>((psj) -> {
            ProductType productType = PositionStaticJoinExtractor.listingProduct_product_productTypeExtractor.get(psj);

            return productType == ProductType.CIV_ETF_EQ || productType == ProductType.CIV_ETF_COMMODITY
              || productType == ProductType.CIV_ETF_FI || productType == ProductType.CIV_ETF_MIXED;
        }, bl), m_cm);
    }

    /**
     * This methods makes sure that the given listener only subscribes to positions in Index
     *
     * @param bl
     */
    private void subscribeToIndexPositions(BulkListener<PositionStaticJoin> bl) {
        m_allPositions.subscribe(new ImmutableFilterListener<>((psj) -> {
            ProductType productType = PositionStaticJoinExtractor.listingProduct_product_productTypeExtractor.get(psj);
            return productType == ProductType.REFERENTIAL_INDEX || productType == ProductType.FUTURE_FINANCIAL;
        }, bl), m_cm);
    }

    private KeysActivator<String> m_etfSubscriber = new KeysActivator<String>() {
        @Override
        public void update(Set<String> activate, Set<String> deactivate) {
            for (String s : activate) {
                m_uji.downloadAndLoadETF(s, DateHelper.getToday());
            }
        }

        @Override
        public void clear() { }
    };
    private KeysActivator<String> m_indexSubscriber = new KeysActivator<String>() {
        @Override
        public void update(Set<String> activate, Set<String> deactivate) {
            for (String s : activate) {
                m_uji.downloadAndLoadIndex(s, DateHelper.getToday());
            }
        }

        @Override
        public void clear() { }
    };

    public static UltumusRequestsMonitor create(ApplicationContext<GHCOComponent> ctx) {
        UltumusDataInjector uji = ctx.get(GHCOComponent.UltumusDataInjector);
        ThreadingContext tc = SimpleThreadingContext.dedicatedTC("UltumusRequestsMonitor").get();
        DataContextAccessor dc = ctx.get(GHCOComponent.SharedDataContext);
        CacheTable<PositionStaticJoin, Object> allPositions = dc.getTable(PositionStaticJoin.class);
        return new UltumusRequestsMonitor(uji, tc, allPositions);
    }
}
