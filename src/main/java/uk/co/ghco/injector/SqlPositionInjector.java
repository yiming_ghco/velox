package uk.co.ghco.injector;

import com.aralis.application.ApplicationContext;
import com.aralis.df.cache.CachePublisher;
import com.aralis.strings.StringUtil;
import uk.co.ghco.api.CreateTradeBuilder;
import uk.co.ghco.api.Position;
import uk.co.ghco.api.Side;
import uk.co.ghco.api.TradeSource;
import uk.co.ghco.api.ghcoengine.client.GHCOEngineSnapshotClient;
import uk.co.ghco.application.GHCOComponent;

import java.sql.*;
import java.time.LocalDate;

public class SqlPositionInjector {
     final static String s_dbClass = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
     final static String s_jdbcUrl = "jdbc:sqlserver://etf-sql:1433;database=GHCOpositions";
     final static String s_user = "sa";
     final static String s_password="J#rd#n10";
    private final GHCOEngineSnapshotClient m_snapshotClient;
    //CachePublisher<Execution, Object> m_executionSink;
    CachePublisher<Position, Object> m_positionSink;

    public SqlPositionInjector(GHCOEngineSnapshotClient snapshotClient) {
        m_snapshotClient = snapshotClient;
    }

    public static SqlPositionInjector create(ApplicationContext<GHCOComponent> ctx) {
        GHCOEngineSnapshotClient snapshotClient = ctx.get(GHCOComponent.GHCOSnapshotClient);
        return new SqlPositionInjector(snapshotClient);
    }

    public void run() throws SQLException, ClassNotFoundException {
        Class.forName(s_dbClass);

        final Connection conn = DriverManager.getConnection(s_jdbcUrl, s_user, s_password);
        Statement stmt = conn.createStatement();
        String sql = "SELECT * FROM Trading.risk.vwIntradayTrades";
        ResultSet rs = stmt.executeQuery(sql);
        while (rs.next()) {
            String orcId = rs.getString("orc_id");
            if (StringUtil.isEmpty(orcId)) {
                System.out.println(" isin missing for "+ rs.getString("etfCode"));
            }
            m_snapshotClient.sendCommand(CreateTradeBuilder.newBuilder()
                    .externalId(rs.getString("executionId"))
                    .accountId(rs.getString("accountName"))
                    .portfolioName(rs.getString("portfolio"))
                    .tradeSource(TradeSource.ORC)
                    .tradeListingId(orcId)
                    .tradeDate(LocalDate.now())
                    .tradeCcy(rs.getString("currencyCode"))
                    .tradePrice(rs.getDouble("price"))
                    .tradeQty(rs.getDouble("qty"))
                    .side("B".equals(rs.getString("Side"))? Side.Buy:Side.Sell)
                    .get());
        }
    }

    /*public void apply(PositionBuilder builder, Execution execution) {

    }*/
}
