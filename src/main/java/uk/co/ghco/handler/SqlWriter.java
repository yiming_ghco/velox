package uk.co.ghco.handler;

import com.aralis.application.ApplicationContext;
import com.aralis.logging.Logger;
import com.aralis.logging.LoggerFactory;
import uk.co.ghco.api.Side;
import uk.co.ghco.api.Trade;
import uk.co.ghco.application.GHCOComponent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

// This is a sample SQL write class to be called by a client

public class SqlWriter {
    final static String s_dbClass = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    final static String s_jdbcUrl = "jdbc:sqlserver://ghdc-sql-2:1433;database=GHCOpositions";
    final static String s_user = "sa";
    final static String s_password = "";
    private Connection conn = null;
    private final static Logger s_log = LoggerFactory.getLogger();

    public SqlWriter() {
        getConnection();
    }

    public static SqlWriter create(final ApplicationContext<GHCOComponent> ctx) {
        return new SqlWriter();
    }

    private boolean getConnection() {
        try {
            conn = DriverManager.getConnection(s_jdbcUrl, s_user, s_password);
        } catch (Exception e) {
            s_log.info("SQL Exception found in SqlWriter: " + e);
            return false;
        }

        return true;
    }

    public void write(Trade trade) {
        try {
            boolean isBuy = trade.side() == Side.Buy;
            SimpleDateFormat dateTimeFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat dateTimeFormat2 = new SimpleDateFormat("yyyy/MM/dd");
            String timeStamp = dateTimeFormat1.format(new Date());
            String dateStamp = dateTimeFormat2.format(new Date());

            Statement stmt = conn.createStatement();
            String sql = "insert into tIntradayTrades (portfolioID, accountID, codeID, " +
                    "side, currencyID, qty, price, tradeDate,  executionTimeLocal) values ( " + //, exchTranId, tradeDate,  executionTimeLocal,  ) values ("
                    trade.portfolioName() + ","
                    + trade.accountId() + ","
                    + trade.listingId() + ","
                    + (isBuy ? "\'B\'" : "\'S\'") + ","
                    + trade.tradeCcy() + ","
                    + trade.tradeQty() + ","
                    + trade.tradePrice() + ","
              // + trade.orderId() + ","
              + "\'" + dateStamp + "\'" + "," + "\'" + timeStamp + "\'" + ")";

            stmt.executeQuery(sql);

        } catch (Exception e) {
            s_log.info("Exception found SqlWriter Write in SqlWriter: " + e);
        }
    }
}
