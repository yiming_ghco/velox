package uk.co.ghco.web;

import com.aralis.strings.StringUtil;
import com.aralis.vm.ScreenProvider;
import com.aralis.vm.SessionState;
import com.aralis.vm.SimpleScreenProviderFactory;
import uk.co.ghco.configuration.GHCOUserConfigEnum;
import uk.co.ghco.configuration.UserConfigProvider;
import java.util.List;

public class GHCOScreenProviderFactory extends SimpleScreenProviderFactory {
    public GHCOScreenProviderFactory(List<ScreenProvider> provider) {
        super(provider);
    }

    @Override
    public boolean isEntitled(ScreenProvider provider, SessionState state) {
        if (state.getUserId().equals("system")) {
            return true;
        }

        final UserConfigProvider ucp = state.getExisting(UserConfigProvider.class);
        final String entitled = ucp.getConfiguration(GHCOUserConfigEnum.ScreenEntitlement);
        if (!StringUtil.isEmpty(entitled)) {
            if ("ALL".equalsIgnoreCase(entitled.trim())) {
                return true;
            }

            if (entitled.indexOf(provider.name()) != -1) {
                return true;
            }
        }

        return false;
    }
}
