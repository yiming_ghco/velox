package uk.co.ghco.web;

import com.aralis.df.cache.AccumulatedBatch;
import com.aralis.df.cache.CachePublisher;
import com.aralis.df.cache.CachePublisherTrackingFactory;
import com.aralis.df.cache.ConflatedBatchFactory;
import com.aralis.gooey.api.*;
import com.aralis.lists.Pair;
import com.aralis.logging.Logger;
import com.aralis.logging.LoggerFactory;
import com.aralis.strings.StringUtil;
import com.aralis.threads.CancellableSupplier;
import com.aralis.threads.SimpleCancellationManager;
import com.aralis.threads.SimpleThreadingContext;
import com.aralis.threads.ThreadingContext;
import com.aralis.tools.*;
import com.aralis.tools.configuration.BaseConfigAdminImpl;
import com.aralis.tools.configuration.ConfigProviderFactory;
import com.aralis.tools.configuration.EntityNamespaceProvider;
import com.aralis.tools.configuration.UIConfigProvider;
import com.aralis.tools.configuration.ui.ConfigurationUIRegistry;
import com.aralis.tools.password.PasswordEncryptionService;
import com.aralis.tools.teststudio.impl.GooeyMessageRecorder;
import com.aralis.tools.teststudio.impl.SchemaRegistry;
import com.aralis.tools.workspace.ui.ConfigBasedWorkspacePersistenceManager;
import com.aralis.vm.ScreenProvider;
import com.aralis.vm.*;
import com.aralis.vm.workspace.WorkspacePersistenceManager;
import uk.co.ghco.api.GHCOSchemaDescriptor;
import uk.co.ghco.application.GHCOApplication;
import uk.co.ghco.application.UserSettingScreenProvider;
import uk.co.ghco.configuration.GHCOUserConfigEnum;
import uk.co.ghco.configuration.GHCOUserConfigNamespace;
import uk.co.ghco.configuration.UserConfigProvider;
import uk.co.ghco.ui.*;
import uk.co.ghco.util.LanguageLoader;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class GHCOSessionCreateListener implements SessionCreateListener {
    private final static Logger s_log = LoggerFactory.getLogger();
    private final static String s_systemUser = "system";
    private final com.aralis.vm.ScreenProviderFactory m_providerFactory;
    private final String m_instanceSessionId;
    private final Map<String, Map<String, String>> m_languages;
    private final CachePublisher<AralisSession, String> m_sessionInfo;
    private CachePublisher<UserSessionInfo, Object> m_userSessionInfo =
      CachePublisherTrackingFactory.Uninstrumented.create(UserSessionInfo.class, UserSessionInfoExtractor.Primary);
    private final GHCOApplication m_application;
    private final ConfigProviderFactory m_configFactory;
    private final Map<String, GooeyMessageRecorder> m_recorder = new ConcurrentHashMap<>();
    private final SchemaRegistry m_schemaRegistry;

    public GHCOSessionCreateListener(
      GHCOApplication application, List<ScreenProvider> externalProviders) {
        m_application = application;
        m_instanceSessionId = application.getInstanceId() + "_" + Instant.now();

        AralisToolsApplication tools = application.getToolsApplication();
        m_sessionInfo = tools.getAralisSessionInfo();
        m_configFactory = tools.getConfigProviderFactory();

        List<ScreenProvider> screenProviders = new ArrayList<>();
        screenProviders.add(new PositionViewerScreenProvider("Positions", "GHCO", "fa-shopping-basket"));
        screenProviders.add(new PositionHistoryViewerScreenProvider("Position History", "GHCO", "fa-shopping-basket"));
        screenProviders.add(new TradeViewerScreenProvider("Trades", "GHCO", "fa-shopping-basket"));
        screenProviders.add(new TradeEntryScreenProvider("Test Trade",
          "GHCO",
          "fa-shopping-basket",
          m_application.getStateEngineClient()));
        screenProviders.add(new ImpliedPositionViewerScreenProvider(
          "Implied Positions",
          "GHCO",
          "fa-shopping-basket",
          m_application));
        screenProviders.add(new UserSettingScreenProvider("User Settings", "Configuration", "fa-cogs"));
        screenProviders.addAll(externalProviders);

        m_providerFactory = new GHCOScreenProviderFactory(screenProviders);

        m_languages = LanguageLoader.loadLanguages();

        AccumulatedBatch<UserSessionInfo, Object> accumulator =
          ConflatedBatchFactory.create(UserSessionInfoExtractor.Primary, (published, changes, removed) -> {
              published.forEach(info -> tools.getToolsViewClient().submitUserSessionInfo(info));
              changes.forEach(pair -> tools.getToolsViewClient().submitUserSessionInfo(pair.getNew()));
          });

        m_userSessionInfo.getTable().subscribe(accumulator, new SimpleCancellationManager());
        SimpleThreadingContext.scheduleAtFixedRate(r -> r.run(), () -> accumulator.flush(), 10, TimeUnit.SECONDS);

        m_schemaRegistry = new SchemaRegistry();
        m_schemaRegistry.registerSchema(GHCOSchemaDescriptor.SchemaDescriptor);
        m_schemaRegistry.registerSchema(AralisToolsSchemaDescriptor.SchemaDescriptor);
    }

    public com.aralis.vm.ScreenProviderFactory getProviderFactory() {
        return m_providerFactory;
    }

    @Override
    public LoginRequestListener create(String sessionId) {
        return new LoginRequestHandler(sessionId, null);
    }

    @Override
    public LayoutLoader layoutLoader() {
        return screen -> LayoutHelper.loadURLLayout("html", screen);
    }

    @Override
    public void created(final AralisSession session) {
        s_log.info(session.sessionId(), "created");
        m_sessionInfo.publish(session);
    }

    @Override
    public void destroyed(final AralisSession session) {
        s_log.info(session.sessionId(), "destroyed");
        String user = "";
        // update the entry in UserSessionInfo table
        String sessionId = session.sessionId();
        AralisToolsApplication tools = m_application.getToolsApplication();
        for (UserSessionInfo info : tools.getToolsViewClient().getUserSessionInfoTable().values()) {
            if (info.sessionId().equals(sessionId) && info.systemSessionId().equals(m_instanceSessionId)) {
                UserSessionInfoPOJOImplBuilder builder = new UserSessionInfoPOJOImplBuilder(info);
                builder.logoutTime(Instant.now());
                tools.getToolsViewClient().submitUserSessionInfo(builder.get());
                user = info.userId();
                break;
            }
        }
        GooeyMessageRecorder recorder = m_recorder.get(sessionId);
        if (recorder != null) {
            recorder.stopRecording();
            m_recorder.remove(sessionId);
        }
        final AralisSession aralisWebSession = m_sessionInfo.getTable().value(session.sessionId());
        aralisWebSession.onClose();
        m_sessionInfo.eventWithKeys(Collections.emptyList(), Collections.singleton(session.sessionId()));
        s_log.info("Session [", session.sessionId(), "] has ended for user [", user, "]");
    }

    private class LoginRequestHandler implements LoginRequestListener {
        private final String m_sessionId;
        private final CancellableSupplier<ThreadingContext> m_tc;

        LoginRequestHandler(String sessionId, CancellableSupplier<ThreadingContext> tc) {
            m_sessionId = sessionId;
            m_tc = tc;
        }

        @Override
        public Pair<LoginResponse, SessionState> process(LoginRequest request) {
            final String user = request.userName();
            final String password = request.password();
            try {
                if (StringUtil.isEmpty(user) && StringUtil.isEmpty(password)) {
                    return handleAnonymousLogin();
                } else {
                    return handleUserLogin(user, password);
                }
            } catch (Exception ex) {
                s_log.info("error when authenticating user", user, ex);
                return failure("error when authenticating " + user + ". Please contact support");
            }
        }

        private Pair<LoginResponse, SessionState> handleAnonymousLogin() {
            final UserConfigProvider ucp = UserConfigProvider.create(s_systemUser, m_configFactory);

            if (ucp.isValid()) {
                s_log.error("user '" + s_systemUser + "' has been setup, anonymous login is no longer allowed");
                return failure("Anonymous login is no longer allowed");
            }

            s_log.error("anonymous login allowed. To block access, set up a user with name '" + s_systemUser + "'");
            return success(sessionState(m_sessionId, s_systemUser, null, m_tc),
              "Setup user " + s_systemUser + " to block anonymous access");
        }

        private Pair<LoginResponse, SessionState> handleUserLogin(String user, String password)
          throws InvalidKeySpecException, NoSuchAlgorithmException {
            final UserConfigProvider ucp = UserConfigProvider.create(user, m_configFactory);
            if (!ucp.isValid()) {
                s_log.info("user", user, "has not been setup to use the system");
                return failure("You are not allowed to access the system");
            }

            UserLogin login = ucp.getConfiguration(GHCOUserConfigEnum.Password, UserLogin.class);

            if (login == null && StringUtil.isEmpty(password)) {
                s_log.info("Letting the user login as the id is set up but no password is set: " + user);
                return success(sessionState(m_sessionId, user, login, m_tc), "Password not set.");
            } else if (login == null) {
                return failure("Incorrect password.");
            }

            final PasswordEncryptionService service = new PasswordEncryptionService();
            final Base64.Decoder decoder = Base64.getDecoder();

            if (!service.authenticate(password, decoder.decode(login.hash()), decoder.decode(login.salt()))) {
                s_log.info("Login failed for user: " + user);
                return failure("Incorrect password.");
            }

            s_log.info("user " + user + " logged in.");

            final AralisSession session = m_sessionInfo.getTable().value(m_sessionId);

            session.subscribeSessionStats(stats -> {
                UserSessionInfo info = new UserSessionInfoPOJOImplBuilder().systemSessionId(m_instanceSessionId)
                  .sessionId(stats.sessionId())
                  .remoteAddress(stats.remote())
                  .userId(stats.userId())
                  .loginTime(stats.loginTime())
                  .pingCount(stats.pingCount())
                  .roundTrip(stats.roundTripMillis())
                  .userAgent(stats.userAgent())
                  .transport(stats.transport())
                  .protocol(stats.protocol())
                  .get();
                m_userSessionInfo.publish(info);
            });

            SessionState sessionState = sessionState(m_sessionId, user, login, m_tc);
            return success(sessionState, "");
        }

        private void createUser(String user, String group) {
            BaseConfigAdminImpl<GHCOUserConfigNamespace> configAdmin =
              m_configFactory.getBaseConfigAdmin(GHCOUserConfigNamespace.class.getSimpleName());
            configAdmin.updateConfig(user, GHCOUserConfigEnum.UserId.toString(), user);
            configAdmin.updateConfig(user, GHCOUserConfigEnum.Group.toString(), group);
        }

        private SessionState sessionState(
          String sessionId, String userName, UserLogin login, CancellableSupplier<ThreadingContext> userThread) {
            if (userThread == null) {
                userThread = SimpleThreadingContext.dedicatedTC("session-" + sessionId + " (" + userName + ")");
            }
            final String language = System.getProperty("language.default", "English"); // TODO configurable

            final SessionState state =
              new SessionState(userName, sessionId, userThread, m_application.sharedUserState(userName));

            state.getManager(UserLogin.class, s -> login);

            state.getManager(LanguageAttributes.class, s -> new LanguageAttributes(language, m_languages));

            state.getManager(EntityNamespaceProvider.class,
              s -> new EntityNamespaceProvider(m_application.getInstanceId(), m_configFactory));

            state.getManager(CachePublisherTrackingFactory.class,
              s -> m_application.getToolsApplication().getCachePublisherRegistery(s));
            final DomainControlActionHandlerRegistry actionHandlerRegistry =
              state.getManager(DomainControlActionHandlerRegistry.class, t -> new ConfigurationUIRegistry());
            m_application.getToolsApplication().registerControlActionHandler(actionHandlerRegistry);

            state.getManager(WorkspacePersistenceManager.class,
              s -> new ConfigBasedWorkspacePersistenceManager(s.getUserId(),
                (UIConfigProvider) EntityNamespaceProvider.getUiConfig(s)));

            state.getManager(AppMode.class, s -> {
                UserConfigProvider ucp = state.getExisting(UserConfigProvider.class);
                AppMode mode = ucp != null ?
                  ucp.getConfiguration(GHCOUserConfigEnum.AppMode, AppMode.class, AppMode.SPA) :
                  AppMode.SPA;
                s_log.info("mode is " + mode);
                return mode;
            });

            return state;
        }

        @Override
        public void postLogin(ClientNotifier notifier, SessionState sessionState) {
        }

        private Pair<LoginResponse, SessionState> failure(String message) {
            return new Pair<>(LoginResponseBuilder.newBuilder()
              .loginResponseType(LoginResponseType.FAILURE)
              .message(message)
              .get(), null);
        }

        private Pair<LoginResponse, SessionState> success(SessionState state, String message) {
            return new Pair<>(LoginResponseBuilder.newBuilder()
              .loginResponseType(LoginResponseType.SUCCESS)
              .message(message)
              .get(), state);
        }
    }

}
