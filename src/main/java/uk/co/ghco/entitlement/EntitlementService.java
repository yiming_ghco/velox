package uk.co.ghco.entitlement;

import com.aralis.application.ApplicationContext;
import com.aralis.config.EntityConfigProvider;
import com.aralis.df.cache.filter.Filter;
import com.aralis.df.cache.state.DataContextAccessor;
import com.aralis.df.cache.state.FilteredDataContext;
import com.aralis.df.cache.state.FilteredDataContextAccessor;
import com.aralis.logging.Logger;
import com.aralis.logging.LoggerFactory;
import org.eclipse.collections.impl.map.mutable.ConcurrentHashMap;
import org.eclipse.collections.impl.map.mutable.UnifiedMap;
import uk.co.ghco.api.PositionJoin;
import uk.co.ghco.api.Side;
import uk.co.ghco.api.TradeJoin;
import uk.co.ghco.application.GHCOComponent;
import uk.co.ghco.configuration.GHCOUserConfigEnum;
import uk.co.ghco.configuration.GHCOUserConfigNamespace;
import java.util.Map;

import static com.aralis.lists.CollectionUtil.getOrCreate;

public class EntitlementService {
    static private final Logger s_log = LoggerFactory.getLogger();
    final private Map<String, DataContextAccessor> m_entitledUserDataContextMap = new ConcurrentHashMap<>();
    final private Map<String, Map<Class, FilteredDataContextAccessor<?>>> m_dataContextFilterMap =
      new ConcurrentHashMap<>();

    public DataContextAccessor getEntitledDataContext(
      final EntityConfigProvider<GHCOUserConfigNamespace> ecp, final DataContextAccessor dc) {
        String userId = ecp.getConfiguration(GHCOUserConfigEnum.UserId.toString());
        return getOrCreate(m_entitledUserDataContextMap, userId, () -> {
            // entitled data context is chained
            FilteredDataContextAccessor<?> fdc = createFilteredDataContext(ecp, dc, PositionJoin.class);
            fdc = createFilteredDataContext(ecp, fdc, TradeJoin.class);
            return fdc;
            //            return dc;
        });
    }

    public static EntitlementService create(ApplicationContext<GHCOComponent> ctx) {
        return new EntitlementService();
    }

    private <T> FilteredDataContextAccessor<T> createFilteredDataContext(
      final EntityConfigProvider<GHCOUserConfigNamespace> ecp, final DataContextAccessor dc, Class<T> clz) {
        s_log.info("Create FilteredDataContextAccessor for class " + clz.getSimpleName());
        String userId = ecp.getConfiguration(GHCOUserConfigEnum.UserId.toString());
        Map<Class, FilteredDataContextAccessor<?>> map = getOrCreate(m_dataContextFilterMap, userId, UnifiedMap::new);

        FilteredDataContextAccessor<?> fdc = map.get(clz);
        if (fdc == null) {
            Filter filter = buildFilter(ecp, clz);
            if (filter != null) {
                fdc = dc.getFilteredContext(clz, filter, userId);
                map.put(clz, fdc);
            } else {
                s_log.info("Unable to build filter.");
            }
            return (FilteredDataContext<T>) fdc;
        } else {
            throw new RuntimeException("Unexpected repeated class when creating filtered data context" + clz);
        }
    }

    public Filter buildFilter(
      final EntityConfigProvider<GHCOUserConfigNamespace> ecp, Class clz) {
        String group = ecp.getConfiguration(GHCOUserConfigEnum.Group.toString());
        if (clz == PositionJoin.class) {
            return new Filter<PositionJoin>() {
                @Override
                public boolean test(final PositionJoin pos) {
                    if ("Management".equals(group)) {
                        return true;
                    }
                    if ("Test".equals(group)) {
                        return pos.psj().position().position() <= 1000;
                    }
                    return true;
                }
            };
        } else if (clz == TradeJoin.class) {
            return new Filter<TradeJoin>() {
                @Override
                public boolean test(final TradeJoin trade) {
                    if ("Management".equals(group)) {
                        return true;
                    }
                    if ("Test".equals(group)) {
                        return trade.trade().side() == Side.Buy;
                    }
                    return true;
                }
            };
        }
        return null;
    }
}
