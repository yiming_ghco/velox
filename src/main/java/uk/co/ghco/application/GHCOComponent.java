package uk.co.ghco.application;

public enum GHCOComponent {
    InstanceId,
    AralisToolsApplication,
    GHCO,
    AppConfig,
    SharedDataContext,
    EntitlementService,
    SimulatedDataInjector,
    GHCOSnapshotClient,
    SqlPositionInjector,
    SqlProductInjector,
    UltumusDataInjector,
    RawJSONStore,
    ReferenceDataStore,
    BasketConstituentsLoader,
    SQLWriter,
    SEClientSql,
    UltumusRequestsMonitor,
    ImpliedPositionCalculator,
    SODPositionsInjector
}
