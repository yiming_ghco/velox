package uk.co.ghco.application;

import com.aralis.application.ApplicationContext;
import com.aralis.config.EntityConfigProvider;
import com.aralis.df.cache.state.DataContextAccessor;
import com.aralis.logging.Logger;
import com.aralis.logging.LoggerFactory;
import com.aralis.persistence.RecordConsumer;
import com.aralis.persistence.json.JSONReader;
import com.aralis.strings.StringUtil;
import com.aralis.tools.AralisConfig;
import com.aralis.tools.AralisConfigPOJOImplBuilder;
import com.aralis.tools.AralisToolsApplication;
import com.aralis.tools.AralisToolsSchemaDescriptor;
import com.aralis.tools.configuration.ui.GridConfigUIPermissionProvider;
import com.aralis.vm.SessionState;
import com.velox.data.injector.BasketConstituentsLoader;
import com.velox.data.injector.UltumusDataInjector;
import uk.co.ghco.api.ghcoengine.client.GHCOEngineSnapshotClient;
import uk.co.ghco.configuration.GHCOAppConfigEnum;
import uk.co.ghco.configuration.GHCOAppConfigNamespace;
import uk.co.ghco.configuration.GHCOUserConfigEnum;
import uk.co.ghco.configuration.UserConfigProvider;
import uk.co.ghco.entitlement.EntitlementService;
import uk.co.ghco.util.FileHelper;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * This is a facade to access various components commonly needed by ScreenProviders etc...
 */
public class GHCOApplication {
    @SuppressWarnings("unused")
    private final static Logger s_log = LoggerFactory.getLogger();
    private final Map<String, SessionState> m_sharedUserState = new HashMap<>();
    private final ApplicationContext<GHCOComponent> m_ctx;
    private final GHCOEngineSnapshotClient m_stateEngineClient;

    private GHCOApplication(
      ApplicationContext<GHCOComponent> ctx) {
        m_ctx = ctx;
        // if configuration server doesn't have anything populated, load configuration from bootstrap file
        if (getToolsApplication().getToolsViewClient().getAralisConfigTable().values().isEmpty()) {
            JSONReader reader = AralisToolsSchemaDescriptor.SchemaJSONReader;
            String cfgFile =
              FileHelper.readFile("/bootstrap.cfg", Charset.defaultCharset(), this.getClass().getClassLoader());
            if (!StringUtil.isEmpty(cfgFile)) {
                reader.startReader(cfgFile, new ConfigConsumer());
            }
        }
        m_stateEngineClient = m_ctx.get(GHCOComponent.GHCOSnapshotClient);
    }

    class ConfigConsumer implements RecordConsumer {
        @Override
        public <T> void deliver(Class<T> type, T record, ByteBuffer serializedForm) {
            if (type.equals(AralisConfig.class)) {
                final AralisConfigPOJOImplBuilder builder = new AralisConfigPOJOImplBuilder((AralisConfig) record);
                builder.aralisIdClear();
                getToolsApplication().getToolsViewClient().submitAralisConfig(builder.get());
            }
        }
    }

    public static GHCOApplication create(ApplicationContext<GHCOComponent> ctx) {
        return new GHCOApplication(ctx);
    }

    public String getInstanceId() {
        return m_ctx.get(GHCOComponent.InstanceId);
    }

    public AralisToolsApplication getToolsApplication() {
        return m_ctx.get(GHCOComponent.AralisToolsApplication);
    }

    public EntityConfigProvider<GHCOAppConfigNamespace> getAppConfig() {
        return m_ctx.get(GHCOComponent.AppConfig);
    }

    public GHCOEngineSnapshotClient getStateEngineClient() { return m_stateEngineClient; }

    public UltumusDataInjector getUltumusDataInjector() { return m_ctx.get(GHCOComponent.UltumusDataInjector); }

    public BasketConstituentsLoader getBasketConstituentsLoader() { return m_ctx.get(GHCOComponent.BasketConstituentsLoader); }

    public SessionState sharedUserState(String userId) {
        EntitlementService entitlementService = m_ctx.get(GHCOComponent.EntitlementService);
        synchronized (m_sharedUserState) {
            return m_sharedUserState.computeIfAbsent(userId, id -> {
                final SessionState state = new SessionState(id);
                final UserConfigProvider ucp = state.getManager(
                  UserConfigProvider.class,
                  us -> UserConfigProvider.create(userId, getToolsApplication().getConfigProviderFactory()));

                state.getManager(GridConfigUIPermissionProvider.class, us -> item -> {
                    switch (item) {
                        case ManageToolbar:
                        case ManageContextMenu:
                        default:
                            return "true".equalsIgnoreCase(ucp.getConfiguration(GHCOUserConfigEnum.AllowAdvancedGridManagement));
                    }
                });
                if (userId.equals("system")) {
                    state.getManager(DataContextAccessor.class, t -> getDataContext(null));
                } else {
                    state.getManager(
                      DataContextAccessor.class,
                      t -> entitlementService.getEntitledDataContext(ucp, getDataContext(null)));
                }
                return state;
            });
        }
    }

    public DataContextAccessor getDataContext(String userId) {
        if (StringUtil.isEmpty(userId)) {
            return m_ctx.get(GHCOComponent.SharedDataContext);
        } else {
            SessionState ss = sharedUserState(userId);
            if (ss != null) {
                return sharedUserState(userId).getExisting(DataContextAccessor.class);
            }
            return null;
        }
    }

    @SuppressWarnings("unused")
    private static String getConfiguration(
      GHCOAppConfigEnum key, ApplicationContext<GHCOComponent> context) {
        EntityConfigProvider<GHCOAppConfigNamespace> config = context.get(GHCOComponent.AppConfig);
        if (config != null) {
            return config.getConfiguration(key.toString());
        }
        return null;
    }
}
