package uk.co.ghco.application;

import com.aralis.config.EntityConfigProvider;
import com.aralis.gooey.api.AppMode;
import com.aralis.logging.Logger;
import com.aralis.logging.LoggerFactory;
import com.aralis.persistence.json.JSONPersisterBuilder;
import com.aralis.strings.StringUtil;
import com.aralis.tools.ScreenConfigurator;
import com.aralis.tools.UserLogin;
import com.aralis.tools.UserLoginExtractor;
import com.aralis.tools.UserLoginPOJOImplBuilder;
import com.aralis.tools.configuration.BaseConfigAdmin;
import com.aralis.tools.configuration.EntityNamespaceProvider;
import com.aralis.tools.configuration.ui.ConfigurationEntityEditManager;
import com.aralis.tools.configuration.ui.UIConfigNamespace;
import com.aralis.tools.password.ApiKeyGenerator;
import com.aralis.tools.password.PasswordEncryptionService;
import com.aralis.tools.util.OKCancelHelper;
import com.aralis.vm.BaseScreenProvider;
import com.aralis.vm.ClientNotifier;
import com.aralis.vm.SessionState;
import uk.co.ghco.api.UserSettingScreen;
import uk.co.ghco.configuration.GHCOUserConfigEnum;
import uk.co.ghco.configuration.GHCOUserConfigNamespace;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Base64;

public class UserSettingScreenProvider extends BaseScreenProvider<UserSettingScreen> {
    private final static Logger s_log = LoggerFactory.getLogger();

    public UserSettingScreenProvider(final String caption, final String group, final String icon) {
        super(UserSettingScreen.class, caption, group, icon);
    }

    @Override
    public void create(SessionState sessionState, ClientNotifier clientNotifier) {
        final EntityConfigProvider<UIConfigNamespace> ecp = EntityNamespaceProvider.getUiConfig(sessionState);

        final UserSettingScreen screen =
          new UserSettingScreen(sessionState.getThreadingContext(), ScreenConfigurator.create(ecp));

        OKCancelHelper helper = new OKCancelHelper(clientNotifier, ecp, screen);

        EntityNamespaceProvider enp = sessionState.getExisting(EntityNamespaceProvider.class);
        EntityConfigProvider<GHCOUserConfigNamespace> userConfig =
          enp.getEntityConfig(sessionState.getUserId(), GHCOUserConfigNamespace.class);
        String modeStr = userConfig.getConfiguration(GHCOUserConfigEnum.AppMode.toString());
        screen.m_appMode.setOptions(Arrays.asList(AppMode.SPA.toString(), AppMode.MPA.toString()));
        screen.m_appMode.setValue(modeStr);

        UserLogin ul = userConfig.getConfiguration(GHCOUserConfigEnum.Password.toString(), UserLogin.class);
        screen.m_restApiKey.setValue(ul == null ? null : ul.apiKey());

        screen.m_applyChange.setListener(t -> {
            BaseConfigAdmin<GHCOUserConfigNamespace> configAdmin =
              enp.getConfigAdmin(GHCOUserConfigNamespace.class);
            configAdmin.updateConfig(sessionState.getUserId(),
              GHCOUserConfigEnum.AppMode.toString(),
              screen.m_appMode.getValue());

            helper.show("App mode changed. Please log out and log back in for the change to take effect. ");
        });

        screen.m_changePassword.setListener(t -> {
            if (StringUtil.isEmpty(screen.m_newPassword.getValue())) {
                helper.show("New password can't be empty.");
                return;
            } else if (!screen.m_newPassword.getValue().equals(screen.m_newPasswordConfirm.getValue())) {
                helper.show("New passwords don't match.");
                return;
            }

            try {
                UserLogin login =
                  userConfig.getConfiguration(GHCOUserConfigEnum.Password.toString(), UserLogin.class);
                if (login != null) {
                    PasswordEncryptionService service = new PasswordEncryptionService();
                    Base64.Decoder decoder = Base64.getDecoder();
                    if (service.authenticate(screen.m_oldPassword.getValue(),
                      decoder.decode(login.hash()),
                      decoder.decode(login.salt()))) {
                        final String newPassword = screen.m_newPassword.getValue();
                        final String apikey = login == null ? null : login.apiKey();
                        final LocalDate expirationDate = login == null ? null : login.accountExpiryDate();
                        login = ConfigurationEntityEditManager.createUserLogin(sessionState.getUserId(),
                          newPassword,
                          apikey,
                          expirationDate);
                        BaseConfigAdmin<GHCOUserConfigNamespace> configAdmin =
                          enp.getConfigAdmin(GHCOUserConfigNamespace.class);
                        final JSONPersisterBuilder persisterBuilder = new JSONPersisterBuilder();
                        persisterBuilder.registerClazz(UserLogin.class, UserLoginExtractor.Extractors);
                        configAdmin.updateConfig(sessionState.getUserId(),
                          GHCOUserConfigEnum.Password.toString(),
                          persisterBuilder.persistOne(UserLogin.class, login));
                        helper.show("Password changed successfully.");
                    } else {
                        helper.show("Incorrect password. ");
                        return;
                    }
                } else {
                    helper.show("Can't find login information for the user.");
                }
            } catch (Exception e) {
                helper.show("Unable to confirm the existing password. Change password failed.");
            }
        });

        screen.m_generateApiKey.setListener(t -> {
            String newRestApiKey = ApiKeyGenerator.generateApiKey(sessionState.getUserId());
            screen.m_restApiKey.setValue(newRestApiKey);
            UserLogin login =
              userConfig.getConfiguration(GHCOUserConfigEnum.Password.toString(), UserLogin.class);
            if (login != null) {
                UserLoginPOJOImplBuilder builder = new UserLoginPOJOImplBuilder(login);
                builder.apiKey(newRestApiKey);
                updatePasswordConfig(builder.get(), sessionState.getUserId(), enp);
            } else {
                helper.show("Can't create API key for user which doesn't exist.");
                return;
            }
        });

        clientNotifier.created(screen);
    }

    private void updatePasswordConfig(UserLogin passwordConfig, String userId, EntityNamespaceProvider enp) {
        BaseConfigAdmin<GHCOUserConfigNamespace> configAdmin =
          enp.getConfigAdmin(GHCOUserConfigNamespace.class);
        final JSONPersisterBuilder persisterBuilder = new JSONPersisterBuilder();
        persisterBuilder.registerClazz(UserLogin.class, UserLoginExtractor.Extractors);
        configAdmin.updateConfig(userId,
          GHCOUserConfigEnum.Password.toString(),
          persisterBuilder.persistOne(UserLogin.class, passwordConfig));
    }
}
