package uk.co.ghco.application;

import com.aralis.df.cache.viewserver.CompositeKeyConverter;
import com.aralis.df.extractor.Converter;
import com.aralis.lists.CollectionUtil;
import com.aralis.logging.Logger;
import com.aralis.logging.LoggerFactory;
import com.aralis.strings.StringUtil;
import com.velox.se.handler.ContextWriter;
import uk.co.ghco.api.*;
import uk.co.ghco.api.ghcoengine.server.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.aralis.object.ObjectUtil.ifNotNull;

public class GHCOCommandHandlerImpl implements GHCOEngineHandler {
    private final static Logger s_log = LoggerFactory.getLogger();
    private final static Map<Class<?>, List<Converter<?, ?>>> s_hashIndicesInfo;

    static {
        Map<Class<?>, List<Converter<?, ?>>> hashIndicesInfo = new HashMap<>();
        hashIndicesInfo.put(Trade.class, Arrays.asList(TradeExtractor.externalIdExtractor));
        hashIndicesInfo.put(Listing.class,
          Arrays.asList(ListingExtractor.orcIdExtractor,
            ListingExtractor.bbgCodeExtractor,
            ListingExtractor.currenexIdExtractor));
        s_hashIndicesInfo = CollectionUtil.readOnly(hashIndicesInfo);
    }

    public static Map<Class<?>, List<Converter<?, ?>>> hashIndicesInfo() { return s_hashIndicesInfo; }

    public String resolveListingId(CreateTrade command, GHCOEngineContext context) {
        if (command.tradeSource() == null) {
            return command.tradeListingId();
        }
        // TODO (GHCO) - here resolve the listing id based on the source on the command
        switch (command.tradeSource()) {
            case ORC:
                return context.getListingByKey(ListingExtractor.orcIdExtractor, command.tradeListingId()).listingId();
            case CURRENEX:
                return context.getListingByKey(ListingExtractor.currenexIdExtractor, command.tradeListingId()).listingId();
            case MANUAL:
            default:
                //TODO (GHCO) tighten the logic here
                return command.tradeListingId();
        }
    }

    @Override
    public void handle(final CreateTrade command, final CreateTradeContext context) {
        PreConditions.checkCreateTrade(command, context);

        // create trade
        TradeBuilder builder = context.trade.create();
        builder.accountId(command.accountId())
          .tradeDate(command.tradeDate())
          .listingId(resolveListingId(command, context))
          .portfolioName(command.portfolioName())
          .side(command.side())
          .tradeCcy(command.tradeCcy())
          .tradeQty(command.tradeQty())
          .tradePrice(command.tradePrice())
          .externalId(command.externalId())
          .version(builder.stateMeta().version())
          .execTime(command.execTime())
          .cancelled(false)
          .algoTradeIndicator(command.algoTradeIndicator())
          .tradeOwner(command.tradeOwner())
          .tradeCounterpart(command.tradeCounterpart())
          .tvtic(command.tvtic())
          .tradeSource(command.tradeSource())
          .pricePendingFlag(command.pricePendingFlag())
          .tradeType(command.tradeType());
        if (builder.execTime() == null) {
            builder.execTime(Instant.now());
        }
        ifNotNull(command.orderId(), builder::orderId);
        ifNotNull(command.versusAccountId(), builder::versusAccountId);
        if (!StringUtil.isEmpty(command.externalId())) {
            builder.tradeId(command.externalId());
        } else {
            builder.tradeId(getNextTradeId(builder));
        }

        // create trade history
        TradeHistoryBuilder tradeHistBuilder = context.tradeHistory.create();
        tradeHistBuilder.tradeId(builder.tradeId()).version(builder.stateMeta().version()).trade(builder.get());

        Trade normalized = normalize(context, builder.get());

        Position curr = position(context, normalized);

        PositionBuilder position = context.position.createOrUpdate(curr);
        position.accountId(normalized.accountId())
          .listingId(normalized.listingId())
          .currency(normalized.tradeCcy())
          .portfolioName(normalized.portfolioName())
          .version(position.stateMeta().version());

        updatePosition(curr, position, normalized);

        PositionHistory currHistory = context.positionHistory.create()
          .positionDate(normalized.tradeDate())
          .tradeId(normalized.tradeId())
          .accountId(normalized.accountId())
          .listingId(normalized.listingId())
          .currency(normalized.tradeCcy())
          .portfolioName(normalized.portfolioName())
          .version(position.stateMeta().version())
          .position(position.get())
          .priorTradeId(curr != null ? curr.lastTradeId() : null)
          .get();

        PositionHistory prevHistory = previous(context, currHistory);
        if (prevHistory != null) {
            context.previousHistory.update(prevHistory).nextTradeId(currHistory.tradeId());
        }
    }

    @Override
    public void handle(final CancelTrade command, final CancelTradeContext context) {
        PreConditions.checkCancelTrade(command, context);

        TradeBuilder trade = context.trade.update(findTrade(command.tradeId(), command.externalRefId(), context));
        trade.cancelled(true).version(trade.stateMeta().version());
        context.tradeHistory.create().tradeId(trade.tradeId()).version(trade.stateMeta().version()).trade(trade.get());

        Trade normalized = normalize(context, trade.get());
        PositionHistory base = baseline(context, normalized);
        PositionHistory head = rebuildHistory(context, base, context.rebuiltHistory);
        PositionBuilder position = context.position.update(position(context, normalized));
        position.version(position.stateMeta().version());
        updatePosition(head.position(), position, null);
    }

    @Override
    public void handle(final CorrectTrade command, final CorrectTradeContext context) {
        PreConditions.checkCorrectTrade(command, context);
        TradeBuilder builder = context.trade.update(findTrade(command.tradeId(), command.externalRefId(), context));
        builder.version(builder.stateMeta().version());
        ifNotNull(command.externalId(), builder::externalId);
        ifNotNull(command.side(), builder::side);
        ifNotNull(command.tradeQty(), builder::tradeQty);
        ifNotNull(command.tradePrice(), builder::tradePrice);
        Trade trade = builder.get();
        context.tradeHistory.create().tradeId(trade.tradeId()).trade(trade).version(trade.stateMeta().version());

        Trade normalized = normalize(context, trade);
        PositionHistory base = baseline(context, normalized);
        PositionHistory head = rebuildHistory(context, base, context.rebuiltHistory);
        PositionBuilder position = context.position.update(position(context, normalized));
        position.version(position.stateMeta().version());
        updatePosition(head.position(), position, null);
    }

    @Override
    public void handle(final CreatePositionSnap command, final CreatePositionSnapContext context) {
        PreConditions.checkCreatePositionSnap(command, context);

        PositionSnapBuilder snap = context.positionSnap.create();
        snap.accountId(command.accountId())
          .listingId(command.listingId())
          .currency(command.currency())
          .quantity(command.quantity())
          .costBasis(command.costBasis())
          .dataSource(command.dataSource())
          .positionSnapType(command.positionSnapType())
          .snapDate(command.snapDate())
          .portfolioName(command.portfolioName())
          .deleted(false);

        PositionBuilder position = context.position.create();
        position.accountId(snap.accountId())
          .listingId(snap.listingId())
          .currency(snap.currency())
          .portfolioName(command.portfolioName())
          .position(snap.quantity())
          .costBasis(snap.costBasis())
          .version(position.stateMeta().version());

        PositionHistoryBuilder history = context.positionHistory.create();
        history.positionDate(command.snapDate())
          .accountId(position.accountId())
          .listingId(position.listingId())
          .currency(position.currency())
          .portfolioName(command.portfolioName())
          .version(position.stateMeta().version())
          .position(position.get());
    }

    private String getNextTradeId(TradeBuilder trade) {
        return "RMS" + trade.stateId().idNum();
    }

    static boolean shouldTrackPosition(String account, GHCOEngineContext context) {
        if (StringUtil.isEmpty(account)) {
            return false;
        }
        return true;
    }

    static Trade findTrade(String tradeId, String externalRefId, GHCOEngineContext context) {
        if (!StringUtil.isEmpty(tradeId)) {
            return context.getTradeByPrimary(tradeId);
        }

        if (!StringUtil.isEmpty(externalRefId)) {
            return context.getTradeByKey(TradeExtractor.externalIdExtractor, externalRefId);
        }

        return null;
    }

    static Side flipSide(Side side) {
        return side == Side.Buy ? Side.Sell : Side.Buy;
    }

    static Position position(GHCOEngineContext context, Trade trade) {
        Object key = PositionExtractor.Primary.keyBuilder()
          .accountId(trade.accountId())
          .listingId(trade.listingId())
          .currency(trade.tradeCcy())
          .portfolioName(trade.portfolioName())
          .get();

        return context.getPositionByPrimary(key);
    }

    /**
     * Apply a trade on the given Position (prior), to get the updated Position (post) Position is a arithmetic series
     * and this method can be viewed as the recursive definition of it:
     * <p>
     * Position{i} = Position{i-1} + Trade{i}
     * <p>
     * where implementation wise Position{i} is broken down into four aspects, each of which follows the same form of
     * definition.
     * <p>
     * This method reads value from prior and trade, and writes value into post. It never reads from post so the initial
     * values stored in post are irrelevant.
     * <p>
     * This method only touches the (quantitative) fields it needs to touch, it will not for example change or clear the
     * post object for unrelated fields.
     *
     * @param prior the Position used as baseline to apply trade to. Null value indicates there is no baseline hence the
     * trade is the very first trade on a given position key.
     * @param post the resulted Position after trade is applied. Cannot be null
     * @param trade the Trade to be applied onto prior Position. Null value is allowed and interpreted as no-op roll
     * over of Position - all the relevant fields will be copied over from prior to post.
     */
    static void updatePosition(Position prior, PositionBuilder post, Trade trade) {
        if (prior == null) {
            prior = s_empty; // all quantitative fields are zero.
        }

        updateBought(prior, post, trade);
        updateSold(prior, post, trade);
        updateCostAndPnL(prior, post, trade);
        updateLastTradeAndPnL(prior, post, trade);
    }

    static void updateBought(Position prior, PositionBuilder post, Trade trade) {
        if (trade == null || trade.cancelled() || trade.side() != Side.Buy) {
            post.boughtQty(prior.boughtQty());
            post.boughtPrice(prior.boughtPrice());
            return;
        }

        // weighted average
        double qty = prior.boughtQty() + trade.tradeQty();
        double px = (prior.boughtQty() * prior.boughtPrice() + trade.tradeQty() * trade.tradePrice()) / qty;
        post.boughtQty(qty);
        post.boughtPrice(px);
    }

    static void updateSold(Position prior, PositionBuilder post, Trade trade) {
        if (trade == null || trade.cancelled() || trade.side() != Side.Sell) {
            post.soldQty(prior.soldQty());
            post.soldPrice(prior.soldPrice());
            return;
        }

        // weighted average
        double qty = prior.soldQty() + trade.tradeQty();
        double px = (prior.soldQty() * prior.soldPrice() + trade.tradeQty() * trade.tradePrice()) / qty;
        post.soldQty(qty);
        post.soldPrice(px);
    }

    static void updateCostAndPnL(Position prior, PositionBuilder post, Trade trade) {
        if (trade == null || trade.cancelled()) {
            post.position(prior.position());
            post.costBasis(prior.costBasis());
            post.realizedPnL(prior.realizedPnL());
            return;
        }

        double tradeQty = sign(trade.side()) * trade.tradeQty();
        if (sameSign(prior.position(), tradeQty)) {
            acquire(prior, post, tradeQty, trade.tradePrice());
        } else {
            double unwindQty = sign(trade.side()) * Math.min(Math.abs(tradeQty), Math.abs(prior.position()));
            unwind(prior, post, unwindQty, trade.tradePrice());
            // in the most complicated case, an unwinding trade will not only unwind, but also flip the side of the
            // position from e.g. long to short or vice versa. In such a case the trade is both unwinding and acquiring
            // hence has to be broken up into two parts. unwinding always goes first, but when start processing the
            // acquiring portion, the baseline image needs to be the result of unwind rather than the original Position
            acquire(post.get(), post, tradeQty - unwindQty, trade.tradePrice());
        }
    }

    static void updateLastTradeAndPnL(Position prior, PositionBuilder post, Trade trade) {
        if (trade == null) {
            post.lastTradeId(prior.lastTradeId());
            post.lastTradeVersion(prior.lastTradeVersion());
            post.lastTradePnL(prior.lastTradePnL());
            return;
        }
        post.lastTradeId(trade.tradeId());
        post.lastTradeVersion(trade.stateMeta().version());
        post.lastTradePnL(post.realizedPnL() - prior.realizedPnL());
    }

    static int sign(Side side) {
        return side == Side.Buy ? 1 : side == Side.Sell ? -1 : 0;
    }

    static boolean sameSign(double a, double b) {
        return (a < 0) == (b < 0);
    }

    /**
     * acquires more position, e.g. the new quantity is to build up the position rather than unwind it. This means if
     * position is long (positive), the new quantity is buy quantity (positive); and if position is short (negative),
     * the new quantity is sell quantity (negative). The side of both the position and quantity is signified by their
     * signs so that the calculation can be applied in general form rather than conditioning on the side.
     * <p>
     * acquire action will move position quantity, change costBasis, but will not yield PnL
     */
    static void acquire(Position prior, PositionBuilder post, double quantity, double price) {
        double qty = prior.position() + quantity;
        double cost = qty == 0 ? 0 : (prior.costBasis() * prior.position() + quantity * price) / qty;
        post.position(qty).costBasis(cost).realizedPnL(prior.realizedPnL());
    }

    /**
     * unwind position, e.g. the new quantity is to trade out the position rather than increasing it. This means if
     * position is long (positive), the new quantity is sell quantity (negative); and if position is short (negative),
     * the new quantity is buy quantity (positive). The side of both the position and quantity is signified by their
     * signs so that the calculation can be applied in general form rather than conditioning on the side.
     * <p>
     * unwind action will move position quantity, yield PnL, but will not change costBasis
     */
    static void unwind(Position prior, PositionBuilder post, double quantity, double price) {
        double qty = prior.position() + quantity;
        double pnl = -quantity * (price - prior.costBasis());
        post.position(qty).costBasis(prior.costBasis()).realizedPnL(prior.realizedPnL() + pnl);
    }

    /**
     * rebuild the Position series (represented by PositionHistory) from a given point onwards. The starting point
     * (from) is excluded and not modified.
     * <p>
     * This method assume some Trade after the given starting point (from) has been modified (canceled or replaced),
     * hence will rerun the updatePosition method for every position historical images, since that point and forward, to
     * fix the historical series until it reaches to latest position data point (e.g. no more PositionHistory going
     * forward)
     *
     * @param context
     * @param from
     * @param writer
     * @return the latest rebuilt PositionHistory (a.k.a the head)
     */
    static PositionHistory rebuildHistory(
      GHCOEngineContext context, PositionHistory from, ContextWriter<PositionHistory, PositionHistoryBuilder> writer) {
        PositionHistory history = from;
        while (history.nextTradeId() != null) {
            PositionHistory nextHistory = next(context, history);
            PositionBuilder nextPosition = PositionBuilder.newBuilder(nextHistory.position());
            Trade nextTrade = normalize(context, context.getTradeByPrimary(nextHistory.tradeId()));
            updatePosition(history.position(), nextPosition, nextTrade);
            history = writer.update(nextHistory).position(nextPosition.get()).get();
        }

        return history;
    }

    // normalize the Trade for position updating purpose. Specifically, flip the account and side for principal fills
    // TODO: we can return two Trade object here for handling internal book transfer
    static Trade normalize(GHCOEngineContext context, Trade trade) {
        if (shouldTrackPosition(trade.versusAccountId(), context)) {
            return TradeBuilder.newBuilder(trade).accountId(trade.versusAccountId()).side(flipSide(trade.side())).get();
        }

        return trade;
    }

    static PositionHistory baseline(GHCOEngineContext context, Trade trade) {
        PositionHistory curr = positionHistory(context,
          trade.tradeDate(),
          trade.tradeId(),
          trade.accountId(),
          trade.listingId(),
          trade.tradeCcy(),
          trade.portfolioName());
        PositionHistory prev = previous(context, curr);
        if (prev != null) {
            return prev;
        }

        return PositionHistoryBuilder.newBuilder()
          .positionDate(trade.tradeDate())
          .accountId(trade.accountId())
          .listingId(trade.listingId())
          .currency(trade.tradeCcy())
          .nextTradeId(trade.tradeId())
          .get();
    }

    static PositionHistory previous(GHCOEngineContext context, PositionHistory history) {
        return positionHistory(context,
          history.positionDate(),
          history.priorTradeId(),
          history.accountId(),
          history.listingId(),
          history.currency(),
          history.portfolioName());
    }

    static PositionHistory next(GHCOEngineContext context, PositionHistory history) {
        return positionHistory(context,
          history.positionDate(),
          history.nextTradeId(),
          history.accountId(),
          history.listingId(),
          history.currency(),
          history.portfolioName());
    }

    static PositionHistory positionHistory(
      GHCOEngineContext context,
      LocalDate date,
      String tradeId,
      String accountId,
      String listingId,
      String currency,
      String portfolioName) {
        return context.getPositionHistoryByPrimary(PositionHistoryExtractor.Primary.keyBuilder()
          .positionDate(date)
          .tradeId(tradeId)
          .accountId(accountId)
          .listingId(listingId)
          .currency(currency)
          .portfolioName(portfolioName)
          .get());
    }

    private static Position s_empty = PositionBuilder.newBuilder().get();

}
