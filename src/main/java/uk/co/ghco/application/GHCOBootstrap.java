package uk.co.ghco.application;

import com.aralis.application.ApplicationContext;
import com.aralis.application.ApplicationContextBuilder;
import com.aralis.application.CoreApplicationContext;
import com.aralis.config.ConfigNamespace;
import com.aralis.config.EntityConfigProvider;
import com.aralis.df.cache.CachePublisherRegistry;
import com.aralis.df.cache.state.DataContextAccessor;
import com.aralis.df.cache.state.DataContextV1;
import com.aralis.df.extractor.TableDescriptor;
import com.aralis.logging.Logger;
import com.aralis.logging.LoggerFactory;
import com.aralis.strings.StringUtil;
import com.aralis.threads.SimpleThreadingContext;
import com.aralis.threads.ThreadingContext;
import com.aralis.tools.AralisToolsApplication;
import com.aralis.tools.configuration.ConfigProviderFactory;
import com.aralis.tools.configuration.ui.UIConfigNamespace;
import com.velox.data.injector.BasketConstituentsLoader;
import com.velox.data.injector.UltumusDataInjector;
import com.velox.data.injector.UltumusURLHelper;
import com.velox.data.reference.api.ReferenceDataSchemaDescriptor;
import com.velox.data.util.StringFileStore;
import com.velox.se.*;
import uk.co.ghco.api.GHCOEngineSEDescriptor;
import uk.co.ghco.api.GHCOSchemaDescriptor;
import uk.co.ghco.api.Trade;
import uk.co.ghco.api.ghcoengine.client.GHCOEngineSnapshotClient;
import uk.co.ghco.configuration.GHCOAppConfigNamespace;
import uk.co.ghco.configuration.GHCOUserConfigNamespace;
import uk.co.ghco.entitlement.EntitlementService;
import uk.co.ghco.handler.SqlWriter;
import uk.co.ghco.implied.ImpliedPositionCalculator;
import uk.co.ghco.injector.*;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static com.aralis.lists.CollectionUtil.getOrCreate;

public class GHCOBootstrap {
    private final static Logger s_log = LoggerFactory.getLogger();

    public static GHCOApplication bootstrap(String instance) {
        final ApplicationContext<CoreApplicationContext> context = AralisToolsApplication.getVanillaContext();
        final AralisToolsApplication tools = context.get(CoreApplicationContext.ToolsApplication);

        final ApplicationContext<GHCOComponent> container = createContext(instance, tools);

        // get any necessary components from the context to bootstrap the app
        SODPositionsInjector positionInjector = container.get(GHCOComponent.SODPositionsInjector);
        SqlProductInjector productInjector = container.get(GHCOComponent.SqlProductInjector);
        GHCOEngineSnapshotClient GHCOEngineSnapshotClientSQLServer = container.get(GHCOComponent.SEClientSql);

        try {
            positionInjector.run();
            productInjector.run();
        } catch (Exception e) {
            s_log.error(e);
        }

        String ultumusURL = System.getProperty("ghco.ultumusurl");
        String ultumusAPIKey = System.getProperty("ghco.ultumusapikey");
        String ultumusClient = System.getProperty("ghco.ultumusclientcode");
        if (StringUtil.isEmpty(ultumusURL) || StringUtil.isEmpty(ultumusAPIKey) || StringUtil.isEmpty(ultumusClient)) {
            s_log.error("Please set properties for ghco.ultumusurl, ghco.ultumusapikey and ghco.ultumusclientcode");
            System.exit(1);
        }
        UltumusURLHelper.setUltumusAPIAttributes(ultumusURL, ultumusClient, ultumusAPIKey);

        UltumusDataInjector ultumusDataInjector = container.get(GHCOComponent.UltumusDataInjector);
        ultumusDataInjector.setPreferredETFs(Arrays.asList("U0000039B5").stream().collect(Collectors.toSet()));
        ultumusDataInjector.run();

        // monitor all positions to automagically requests ETFs and Indices from ultumus
        UltumusRequestsMonitor urm = container.get(GHCOComponent.UltumusRequestsMonitor);

        //        SimulatedDataInjector sdi = container.get(GHCOComponent.SimulatedDataInjector);
        //        sdi.start();

        return container.get(GHCOComponent.GHCO);
    }

    public static ApplicationContext<GHCOComponent> createContext(
      final String instance, final AralisToolsApplication tools) {
        ConfigProviderFactory factory = tools.getConfigProviderFactory();
        factory.registerNamespace(new GHCOUserConfigNamespace(factory));
        factory.registerNamespace(new GHCOAppConfigNamespace(factory));
        factory.setMasterNamespace(UIConfigNamespace.class.getSimpleName(),
          GHCOUserConfigNamespace.class.getSimpleName());

        final ApplicationContextBuilder<GHCOComponent> builder = new ApplicationContextBuilder<>();
        builder.register(GHCOComponent.InstanceId, ctx -> instance);
        builder.register(GHCOComponent.AralisToolsApplication, ctx -> tools);
        builder.register(GHCOComponent.AppConfig, ctx -> createAppConfig(ctx));
        builder.register(GHCOComponent.SharedDataContext, ctx -> createDataContext(ctx));
        builder.register(GHCOComponent.GHCO, ctx -> GHCOApplication.create(ctx));
        builder.register(GHCOComponent.EntitlementService, EntitlementService::create);
        builder.register(GHCOComponent.SimulatedDataInjector, SimulatedDataInjector::create);
        builder.register(GHCOComponent.GHCOSnapshotClient, ctx -> createStateEngineClient(ctx));
        builder.register(GHCOComponent.SODPositionsInjector, SODPositionsInjector::create);
        builder.register(GHCOComponent.SqlProductInjector, SqlProductInjector::create);
        builder.register(GHCOComponent.RawJSONStore, ctx -> createRawJsonStorage(ctx));
        builder.register(GHCOComponent.ReferenceDataStore, ctx -> createReferenceDataStorage(ctx));
        builder.register(GHCOComponent.BasketConstituentsLoader, ctx -> createBasketConstituentsLoader(ctx));
        builder.register(GHCOComponent.UltumusDataInjector, ctx -> createUltumusDataInjector(ctx));
        builder.register(GHCOComponent.UltumusRequestsMonitor, UltumusRequestsMonitor::create);
        builder.register(GHCOComponent.SQLWriter, SqlWriter::create);
        builder.register(GHCOComponent.SEClientSql, ctx -> createStateEngineClientSQL(ctx));
        builder.register(GHCOComponent.ImpliedPositionCalculator, ImpliedPositionCalculator::create);

        return builder.createContext();
    }

    private static StringFileStore createRawJsonStorage(ApplicationContext<GHCOComponent> ctx) {
        return new StringFileStore("." + ctx.get(GHCOComponent.InstanceId) + "/rawJson", true);
    }

    private static StringFileStore createReferenceDataStorage(ApplicationContext<GHCOComponent> ctx) {
        return new StringFileStore("." + ctx.get(GHCOComponent.InstanceId) + "/referenceData", true);
    }

    private static BasketConstituentsLoader createBasketConstituentsLoader(ApplicationContext<GHCOComponent> ctx) {
        DataContextAccessor dc = ctx.get(GHCOComponent.SharedDataContext);
        StringFileStore refDataStore = ctx.get(GHCOComponent.ReferenceDataStore);
        return BasketConstituentsLoader.create(refDataStore, dc, false);
    }

    private static UltumusDataInjector createUltumusDataInjector(ApplicationContext<GHCOComponent> ctx) {
        String instanceId = ctx.get(GHCOComponent.InstanceId);
        boolean loadPreferred = "true".equals(System.getProperty("ghco.loadpreferred"));
        DataContextAccessor dc = ctx.get(GHCOComponent.SharedDataContext);
        StringFileStore rawJsonStore = ctx.get(GHCOComponent.RawJSONStore);
        StringFileStore referenceDataStore = ctx.get(GHCOComponent.ReferenceDataStore);
        BasketConstituentsLoader bcl = ctx.get(GHCOComponent.BasketConstituentsLoader);
        AralisToolsApplication toolsApp = ctx.get(GHCOComponent.AralisToolsApplication);
        CachePublisherRegistry registry = toolsApp.getCachePublisherRegistery();
        return UltumusDataInjector.create(instanceId, loadPreferred, true, // load ETF only
          true, // don't populate product data with basket constituent data
          0, // number of previous days to load
          3600000, // every hour
          false, // whether this is a deterministic instance, dev only
          dc, rawJsonStore, referenceDataStore, bcl, registry, null);
    }

    private static EntityConfigProvider<ConfigNamespace> createAppConfig(ApplicationContext<GHCOComponent> ctx) {
        String instance = ctx.get(GHCOComponent.InstanceId);
        AralisToolsApplication tools = ctx.get(GHCOComponent.AralisToolsApplication);
        ConfigProviderFactory factory = tools.getConfigProviderFactory();
        return factory.getEntityConfigProvider(instance, GHCOAppConfigNamespace.class.getSimpleName());
    }

    private static DataContextAccessor createDataContext(ApplicationContext<GHCOComponent> ctx) {
        AralisToolsApplication tools = ctx.get(GHCOComponent.AralisToolsApplication);
        Map<String, ThreadingContext> tcs = new HashMap<>();
        List<TableDescriptor<?, ?>> descriptors = new ArrayList<>();
        descriptors.addAll(GHCOSchemaDescriptor.Descriptors);
        descriptors.addAll(ReferenceDataSchemaDescriptor.Descriptors);
        return DataContextV1.create(t -> {
            synchronized (tcs) {
                return getOrCreate(tcs,
                  t.getSimpleName(),
                  () -> SimpleThreadingContext.dedicatedTC(t.getSimpleName()).get());
            }
        }, descriptors, tools.getCachePublisherRegistery());
    }

    private static GHCOEngineSnapshotClient createStateEngineClient(ApplicationContext<GHCOComponent> ctx)
      throws IOException, InterruptedException {
        final String instance = ctx.get(GHCOComponent.InstanceId);
        final DataContextAccessor dc = ctx.get(GHCOComponent.SharedDataContext);
        final String session = System.getProperty("ghco.session",
          Instant.now().atZone(ZoneId.systemDefault()).toLocalDate().format(DateTimeFormatter.BASIC_ISO_DATE));

        int port = 11379;
        Connection s_me = ConnectionBuilder.newBuilder().hostName("localhost").port(port).get();

        SharedSEConfig sharedConfig = SharedSEConfigBuilder.newBuilder()
          .instanceName(instance)
          .sessionName(session)
          .connections(Arrays.asList(s_me))
          .commandJournalDurability(WALMode.NSYNC)
          .get();

        SEConfig config =
          SEConfigBuilder.newBuilder().sharedConfig(sharedConfig).myConnection(s_me).preferredPrimary(true).get();

        SimpleThreadingContext.dedicatedTC("GHCOStateEngine").get().accept(() -> {
            try {
                s_log.info("Starting GHCO state engine...");
                GHCOEngineSEDescriptor.startStateEngine(config, Collections.emptyMap(), new GHCOCommandHandlerImpl());
            } catch (Exception e) {
                s_log.error("Unable to start GHCO state engine.", e);
            }
        });
        s_log.info("Waiting for GHCO state engine...");
        Thread.sleep(5000);

        GHCOEngineSnapshotClient client =
          new GHCOEngineSnapshotClient(instance, Arrays.asList(new InetSocketAddress("localhost", port)));
        client.dataContext(dc)
          .subscribePosition()
          .subscribePositionHistory()
          .subscribePositionSnap()
          .subscribeTrade()
          .subscribeTradeHistory()
          .subscribeListing()
          .subscribeProduct();
        client.connect();
        client.awaitUpToDate();

        return client;
    }

    private static GHCOEngineSnapshotClient createStateEngineClientSQL(ApplicationContext<GHCOComponent> ctx)
      throws IOException {

        int port = 11379;

        SqlWriter sqlWriter = ctx.get(GHCOComponent.SQLWriter);

        GHCOEngineSnapshotClient client =
          new GHCOEngineSnapshotClient("SQL", Arrays.asList(new InetSocketAddress("localhost", port)));

        client.subscribeTrade((ps, cs, rs) -> {
            for (Trade p : ps) {
                sqlWriter.write(p);
            }
        });

        client.connect();
        client.awaitUpToDate();

        return client;
    }

}
