package uk.co.ghco.application;

import com.aralis.df.cache.viewserver.CompositeKeyConverter;
import com.aralis.df.extractor.Extractor;
import com.aralis.logging.Logger;
import com.aralis.logging.LoggerFactory;
import com.aralis.strings.StringUtil;
import uk.co.ghco.api.*;
import uk.co.ghco.api.ghcoengine.server.GHCOEngineContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static uk.co.ghco.application.GHCOCommandHandlerImpl.shouldTrackPosition;

class PreConditions {
    private static final Logger s_log = LoggerFactory.getLogger();

    private enum Severity {
        WARN,
        REJECT
    }

    static void checkCreatePositionSnap(CreatePositionSnap command, GHCOEngineContext context) {
        checkFieldsMissing(
          Severity.REJECT,
          context,
          command,
          CreatePositionSnapExtractor.accountIdExtractor,
          CreatePositionSnapExtractor.listingIdExtractor,
          CreatePositionSnapExtractor.currencyExtractor);
        checkPositionSnapAlreadyExists(Severity.REJECT, context, command);
        checkFieldsMissing(Severity.WARN, context, command, CreatePositionSnapExtractor.snapDateExtractor);
    }

    static void checkCreateTrade(CreateTrade command, GHCOEngineContext context) {
        checkFieldsMissing(
          Severity.REJECT,
          context,
          command,
          CreateTradeExtractor.accountIdExtractor,
          CreateTradeExtractor.tradeListingIdExtractor,
          CreateTradeExtractor.sideExtractor,
          CreateTradeExtractor.tradeQtyExtractor,
          CreateTradeExtractor.tradePriceExtractor,
          CreateTradeExtractor.tradeCcyExtractor);
        checkTradeAlreadyExists(Severity.REJECT, context, command);
        checkPositionNotTracked(Severity.REJECT, context, command.accountId(), command.versusAccountId());
        checkFieldsMissing(Severity.WARN, context, command, CreateTradeExtractor.tradeDateExtractor);
    }

    static void checkCancelTrade(CancelTrade command, GHCOEngineContext context) {
        checkTradeNotFound(Severity.REJECT, context, command.tradeId(), command.externalRefId());
    }

    static void checkCorrectTrade(CorrectTrade command, GHCOEngineContext context) {
        checkTradeNotFound(Severity.REJECT, context, command.tradeId(), command.externalRefId());
    }

    static void checkPositionSnapAlreadyExists(Severity level, GHCOEngineContext context, CreatePositionSnap command) {
        Object key = PositionSnapExtractor.Primary.keyBuilder()
          .snapDate( command.snapDate())
          .positionSnapType(command.positionSnapType())
          .dataSource( command.dataSource())
          .accountId(command.accountId())
          .listingId(command.listingId())
          .currency(command.currency())
          .portfolioName(command.portfolioName())
          .get();
        if (context.getPositionSnapByPrimary(key) != null) {
            fail(level, "PositionSnap with the same key already exists", key.toString());
        }
    }

    static <T> void checkFieldsMissing(Severity level, GHCOEngineContext context, T command, Extractor<T, ?>... fields) {
        List<String> missings = null;

        for (Extractor<T, ?> it : fields) {
            if (!it.isSet(command) || it.get(command) == null) {
                if (missings == null) {
                    missings = new ArrayList<>();
                }
                missings.add(it.name());
            }
        }
        if (missings != null) {
            String commandName = command.getClass().getSimpleName();
            fail(level, "[ " + String.join(", ", missings) + " ]", "not populated on", commandName);
        }
    }

    static void checkPositionNotTracked(Severity level, GHCOEngineContext context, String primary, String versus) {
        if (!shouldTrackPosition(primary, context) && !shouldTrackPosition(versus, context)) {
            fail(
              level,
              "both primary and versus accounts are not applicable for position tracking, this is most likely a "
                + "mistake, either the command should not be issued in the first place, or account data is incorrect in"
                + " the engine:",
              "primary account = " + primary,
              "versus account = " + versus);
        }
    }

    static void checkTradeAlreadyExists(Severity level, GHCOEngineContext context, CreateTrade command) {
        String tradeId = command.externalId();
        if (StringUtil.isEmpty(tradeId)) {
            return;
        }
        Trade trade = context.getTradeByPrimary(tradeId);
        if (trade == null) {
            return;
        }

        fail(level, "Trade with the a tradeId same as the command's externalId already exists:", tradeId);

    }

    static void checkTradeNotFound(Severity level, GHCOEngineContext context, String tradeId, String externalRefId) {
        if (GHCOCommandHandlerImpl.findTrade(tradeId, externalRefId, context) == null) {
            fail(
              level,
              "cannot find the trade to cancel/amend by neither tradeId nor externalRefId:",
              "tradeId = " + tradeId,
              "externalRefId = " + externalRefId);
        }
    }

    private static void fail(Severity level, Object... data) {
        String msg = Arrays.stream(data).map(Objects::toString).collect(Collectors.joining(" "));
        if (level == Severity.WARN) {
            s_log.error(msg);
            return;
        }

        if (level == Severity.REJECT) {
            throw new RuntimeException(msg);
        }
    }
}
