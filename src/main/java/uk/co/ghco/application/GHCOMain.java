package uk.co.ghco.application;

import com.aralis.df.cache.state.DataContextAccessor;
import com.aralis.logging.Logger;
import com.aralis.logging.LoggerFactory;
import com.aralis.tools.AralisToolsApplication;
import com.aralis.tools.configuration.ui.ConfigurationEditScreenProvider;
import com.aralis.tools.configuration.ui.ConfigurationEntityEditScreenProvider;
import com.aralis.tools.configuration.ui.LookupPathEditScreenProvider;
import com.aralis.tools.datapersistence.DataPersistenceScreenProvider;
import com.aralis.tools.monitor.ui.SystemMonitorViewerScreenProvider;
import com.aralis.tools.monitor.ui.UserSessionViewerScreenProvider;
import com.aralis.tools.support.SupportViewerScreenProvider;
import com.aralis.vm.ScreenProvider;
import global.aralis.web.vertx.VertxServerBuilder;
import uk.co.ghco.api.GHCOPersistenceHelper;
import uk.co.ghco.web.GHCOSessionCreateListener;
import java.util.Arrays;
import java.util.List;

public class GHCOMain {
    private final static Logger s_log = LoggerFactory.getLogger();
    private final static String WEBROOT = "src/main/webapp";
    private final String m_instanceName;
    private final GHCOApplication m_application;
    private int m_httpPort;
    private int m_httpsPort;
    private String m_keyStorePath;
    private String m_keyStorePassword;
    private String m_contextPath;
    private String m_welcomeFile;

    public GHCOMain(String instanceName) {
        ensureDefaultSystemProperties();
        m_instanceName = instanceName;
        m_application = GHCOBootstrap.bootstrap(m_instanceName);
    }

    public DataContextAccessor getDataContext(String userId) {
        return m_application.getDataContext(userId);
    }

    public void setHttpPort(int port) {
        m_httpPort = port;
    }

    public void setHttpsPort(int port, String keyStorePath, String keyStorePasswd) {
        m_httpsPort = port;
        m_keyStorePath = keyStorePath;
        m_keyStorePassword = keyStorePasswd;
    }

    public void setContextPath(String path, String welcomeFile) {
        m_contextPath = path;
        m_welcomeFile = welcomeFile;
    }

    public boolean run() {
        if (m_httpPort == 0 && m_httpsPort == 0) {
            s_log.error("Http and Https ports haven't been set. ");
            return false;
        }
        final GHCOSessionCreateListener
          listener = new GHCOSessionCreateListener(m_application, outOfBoxProviders());
        final VertxServerBuilder server = new VertxServerBuilder();
        installContextPath(server, m_contextPath, m_welcomeFile, listener);
        if (m_httpPort != 0) {
            server.addPort(m_httpPort);
        }
        if (m_httpsPort != 0) {
            server.addSecurePort(m_httpsPort, m_keyStorePath, m_keyStorePassword);
        }
        server.start();
        return true;
    }

    private List<ScreenProvider> outOfBoxProviders() {
        AralisToolsApplication tools = m_application.getToolsApplication();
        final List<ScreenProvider> screenProviders =
          Arrays.asList(new UserSettingScreenProvider("User Setting", "Configuration", "fa-cogs"),
            new ConfigurationEditScreenProvider(tools, "Config Editor", "Configuration", "fa-cogs"),
            new ConfigurationEntityEditScreenProvider(tools, "Entity Editor", "Configuration", "fa-cogs"),
            new LookupPathEditScreenProvider(tools, "Lookup Path Editor", "Configuration", "fa-cogs"),
            new SupportViewerScreenProvider(tools, "Support Viewer", "Support", "fa-phone"),
            new UserSessionViewerScreenProvider(tools, "User Session", "Support", "fa-users"),
            new DataPersistenceScreenProvider(tools,
              GHCOPersistenceHelper.getTableMap(m_application.getToolsApplication().getToolsViewClient(),
                null,
                m_application.getDataContext(null)),
              GHCOPersistenceHelper.getTableDescriptor(),
              m_application.getDataContext(null),
              "Data Persistence",
              "Support",
              "fa-cogs"),
            new SystemMonitorViewerScreenProvider(tools, "System Monitor", "Support", "fa-stethoscope"));
        return screenProviders;
    }

    private static void installContextPath(
      final VertxServerBuilder builder,
      final String path,
      final String welcomeFile,
      final GHCOSessionCreateListener listener) {
        builder.addWebSocketHandlerWithContextPath(path + "/aralis", listener, listener.getProviderFactory());
        builder.addFileResourceWithContextPath(path, WEBROOT, welcomeFile);
    }

    private static void ensureDefaultSystemProperties() {
        if (System.getProperty("aralis.bootstrap") == null) {
            System.setProperty("aralis.bootstrap", "classpath:bootstrap/localcfg.json");
        }
    }

    public static void main(String[] args) {
        final String instance = System.getProperty("ghco.instance");
        s_log.info("starting instance", instance);

        GHCOMain ghco = new GHCOMain(instance);

        ghco.setHttpPort(7071);
        ghco.setContextPath("/ghco", "index.html");
        ghco.run();
    }
}
