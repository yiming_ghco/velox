package uk.co.ghco.configuration;

import com.aralis.config.ConfigNamespace;
import com.aralis.config.EntityConfigProvider;
import com.aralis.tools.configuration.BaseConfigNamespace;
import com.aralis.tools.configuration.ConfigProviderFactory;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

public class GHCOUserConfigNamespace extends BaseConfigNamespace {

    public GHCOUserConfigNamespace(ConfigProviderFactory factory) {
        super(factory);
        // TODO Auto-generated constructor stub
    }

    @Override
    public <T extends ConfigNamespace, U extends EntityConfigProvider<T>> U createCustomEntityProvider(
        EntityConfigProvider<T> arg0) {
        return null;
    }

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }

    @Override
    public boolean hasCustomAdmin() {
        return false;
    }

    @Override
    public boolean hasCustomProvider() {
        return false;
    }

    @Override
    public boolean isAttributeEnumerated() {
        return true;
    }

    @Override
    public boolean isEntityBased() {
        return true;
    }

    @Override
    protected Collection<String> getLocalAttributes() {
        return Arrays.asList(GHCOUserConfigEnum.values()).stream().map(t -> t.toString()).collect(Collectors.toList());
    }

    @Override
    protected Collection<String> getLocalKeyAttributes() {
        return Arrays
            .asList(GHCOUserConfigEnum.values())
            .stream()
            .filter(t -> t.isKeyAttribute())
            .map(t -> t.toString())
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    protected String getLocalUniqueAttribute() {
        return GHCOUserConfigEnum.UserId.toString();
    }

    @Override
    protected void initialize() {
        // TODO Auto-generated method stub
    }

}
