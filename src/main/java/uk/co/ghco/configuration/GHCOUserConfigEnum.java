package uk.co.ghco.configuration;

public enum GHCOUserConfigEnum {
    UserId(true),
    Region(true),
    Group(true),
    Password(false),
    ScreenEntitlement(false),
    AppMode(false),
    AllowAdvancedGridManagement(false),
    ;

    private boolean m_isKeyAttribute;

    GHCOUserConfigEnum(boolean isKeyAttribute) {
        m_isKeyAttribute = isKeyAttribute;
    }

    boolean isKeyAttribute() {
        return m_isKeyAttribute;
    }
}
