package uk.co.ghco.configuration;

public enum GHCOAppConfigEnum {
    Instance(true),
    Environment(true),
    HttpPort(false),
    HttpsPort(false),

    ;

    final private boolean m_isKeyAttribute;

    GHCOAppConfigEnum(boolean isKeyAttribute) {
        m_isKeyAttribute = isKeyAttribute;
    }

    boolean isKeyAttribute() {
        return m_isKeyAttribute;
    }
}
