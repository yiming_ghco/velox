package uk.co.ghco.configuration;

import com.aralis.config.EntityConfigProvider;
import com.aralis.tools.configuration.ConfigProviderFactory;
import java.util.Map;

public class UserConfigProvider implements EntityConfigProvider<GHCOUserConfigNamespace> {

    private final EntityConfigProvider<GHCOUserConfigNamespace> m_delegate;

    private UserConfigProvider(EntityConfigProvider<GHCOUserConfigNamespace> provider) {
        m_delegate = provider;
    }

    public static UserConfigProvider create(String user, ConfigProviderFactory factory) {
        return new UserConfigProvider(factory.getEntityConfigProvider(user, GHCOUserConfigNamespace.class.getSimpleName()));
    }

    public boolean isValid() {
        return m_delegate != null;
    }

    public String getConfiguration(final GHCOUserConfigEnum attr, final String defaultValue) {
        String val = getConfiguration(attr.toString());
        return val != null ? val : defaultValue;
    }

    public <T extends Enum<T>> T getConfiguration(final GHCOUserConfigEnum attr, Class<T> clazz, final T defaultValue) {
        String val = getConfiguration(attr.toString());
        return val !=null ?  Enum.valueOf(clazz, val) : defaultValue;
    }

    public <T> T getConfiguration(final GHCOUserConfigEnum attr, final Class<T> clazz, final T defaultValue) {
        T val = getConfiguration(attr.toString(), clazz);
        return val != null ? val : defaultValue;
    }

    public String getConfiguration(final GHCOUserConfigEnum attr) {
        return getConfiguration(attr.toString());
    }

    public <T> T getConfiguration(final GHCOUserConfigEnum attr, final Class<T> clazz) {
        return getConfiguration(attr.toString(), clazz);
    }

    @Override
    public String getConfiguration(final String attribute) {
        if (!isValid()) {
            return null;
        }
        return m_delegate.getConfiguration(attribute);
    }

    @Override
    public <T> T getConfiguration(final String attribute, final Class<T> clazz) {
        if (!isValid()) {
            return null;
        }
        return m_delegate.getConfiguration(attribute, clazz);
    }

    @Override
    public Map<String, String> getAllConfigurations() {
        return m_delegate.getAllConfigurations();
    }


}
