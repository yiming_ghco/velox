package uk.co.ghco.marketdata;

import uk.co.ghco.api.PositionStaticJoin;
import uk.co.ghco.api.PositionStaticJoinExtractor;

public class MarketDataUtil {
    public static String resolve(PositionStaticJoin oldS, PositionStaticJoin newS) {
        if (oldS != null && oldS.marketDataId() != null) {
            return oldS.marketDataId();
        } else {
            // TODO (GHCO) plug in more sophisticated logic to resolve the marketDataId
            return PositionStaticJoinExtractor.listingProduct_listing_bbgCodeExtractor.get(newS);
        }
    }
}
