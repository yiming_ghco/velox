package uk.co.ghco.util;

import com.aralis.logging.Logger;
import com.aralis.logging.LoggerFactory;
import com.aralis.strings.StringUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Scanner;

public class FileHelper {
    static final private Logger s_log = LoggerFactory.getLogger();

    static public String readFile(String path, Charset encoding, ClassLoader loader) {
        if (StringUtil.isEmpty(path)) {
            s_log.error(new Exception());
            return "";
        }

        if (path.startsWith("/")) {
            path = path.substring(1);
        }

        try {
            InputStreamReader reader = new InputStreamReader(loader.getResourceAsStream(path), encoding);
            java.util.Scanner s = new java.util.Scanner(reader).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        } catch (Exception e) {
            return null;
        }
    }

    static public InputStream getInputStream(String path, ClassLoader loader) {
        if (StringUtil.isEmpty(path)) {
            s_log.error(new Exception());
            return null;
        }

        if (path.startsWith("/")) {
            path = path.substring(1);
        }

        try {
            return loader.getResourceAsStream(path);
        } catch (Exception e) {
            return null;
        }
    }

    static public String readFile(String path) {
        try {
            InputStream in =  new FileInputStream(path);
            InputStreamReader reader = new InputStreamReader(in);
            Scanner s = new Scanner(reader).useDelimiter("\\A");
            return s.hasNext()? s.next(): "";
        }
        catch (Exception e) {
            s_log.error(e.getMessage());
            return null;
        }
    }

    static public String readFile(File file) {
        try {
            InputStream in =  new FileInputStream(file);
            InputStreamReader reader = new InputStreamReader(in);
            Scanner s = new Scanner(reader).useDelimiter("\\A");
            return s.hasNext()? s.next(): "";
        }
        catch (Exception e) {
            s_log.error(e.getMessage());
            return null;
        }
    }
}

