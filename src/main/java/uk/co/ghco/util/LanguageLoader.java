package uk.co.ghco.util;

import com.aralis.logging.Logger;
import com.aralis.logging.LoggerFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

public class LanguageLoader {

    private final static Logger s_log = LoggerFactory.getLogger();

    public static Map<String, Map<String, String>> loadLanguages() {
        final Map<String, Map<String, String>> languages = new HashMap<>();

        getPropertiesFiles().stream().forEach(filename -> {
            try {
                final Map<String, String> languageMap = new HashMap<>();
                languages.put(languageName(filename), languageMap);
                final Properties prop = new Properties();
                prop.load(getReader(filename));
                prop.forEach((key, value) -> languageMap.put(key.toString(), value.toString()));
            } catch (UnsupportedEncodingException e) {
                s_log.error("/languages/" + filename + " encoding error");
            } catch (IOException e) {
                s_log.error("/languages/" + filename + " load failed");
            }
        });

        return languages;
    }

    private static List<String> getPropertiesFiles() {
        try {
            final URL dirURL = LanguageLoader.class.getClassLoader().getResource("languages");
            if (dirURL == null || !dirURL.getProtocol().equals("file")) {
                throw new NoSuchElementException();
            }
            final String[] fileNames = new File(dirURL.toURI()).list();
            if (fileNames.length == 0) {
                throw new NoSuchElementException();
            }
            return Arrays.asList(fileNames);
        } catch (NoSuchElementException | URISyntaxException e) {
            s_log.error("failed to getResources for /languages");
            return new ArrayList<>();
        }
    }

    private static InputStreamReader getReader(final String fileName) throws UnsupportedEncodingException {
        assert fileName != null && fileName.endsWith(".properties");
        return new InputStreamReader(
            LanguageLoader.class.getClassLoader().getResourceAsStream("languages/" + fileName),
            "UTF-8");
    }

    private static String languageName(final String PropertiesFileName) {
        assert PropertiesFileName != null && !PropertiesFileName.isEmpty();
        return PropertiesFileName.substring(0, PropertiesFileName.lastIndexOf('.'));
    }
}
