package uk.co.ghco.util;

import com.aralis.df.view.ViewDefinition;
import com.aralis.gooey.api.*;
import com.aralis.vm.DataGrid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

public class ConditionalFormatHelper {
    static public class ViewConfigUpdateListener implements BiConsumer<DataGrid, ViewConfig> {
        ViewDefinition m_viewDef;

        public ViewConfigUpdateListener(ViewDefinition viewDef) {
            m_viewDef = viewDef;
        }

        @Override
        public void accept(DataGrid t, ViewConfig u) {
            t.setViewConfigListener(null);
            List<GridFormat> formats = new ArrayList<>();
            formats.addAll(u.formats() == null ? Collections.emptyList() : u.formats());
            if (m_viewDef.getColumns().get("country") != null) {
                formats.add(ConditionalFormatHelper.createCountryFormat("country"));
            }
            if (m_viewDef.getColumns().get("tradingCountry") != null) {
                formats.add(ConditionalFormatHelper.createCountryFormat("tradingCountry"));
            }
            if (m_viewDef.getColumns().get("exchangeCountry") != null) {
                formats.add(ConditionalFormatHelper.createCountryFormat("exchangeCountry"));
            }
            if (m_viewDef.getColumns().get("countryOfIncorporation") != null) {
                formats.add(ConditionalFormatHelper.createCountryFormat("countryOfIncorporation"));
            }
            if (m_viewDef.getColumns().get("domicility") != null) {
                formats.add(ConditionalFormatHelper.createCountryFormat("domicility"));
            }
            if (m_viewDef.getColumns().get("side") != null) {
                formats.add(ConditionalFormatHelper.createBuySellFormat("side"));
            }
            if (m_viewDef.getColumns().get("pctExecuted") != null) {
                formats.add(ConditionalFormatHelper.createLeveledProgressFormat("green", "pctExecuted"));
            }
            if (m_viewDef.getColumns().get("pctSplit") != null) {
                formats.add(ConditionalFormatHelper.createLeveledProgressFormat("blue", "pctSplit"));
            }
            if (m_viewDef.getColumns().get("bid") != null) {
                formats.addAll(ConditionalFormatHelper.createFlashingFormat("bid", "ask", "last"));
            }
            if (m_viewDef.getColumns().get("pricerNavLast") != null) {
                formats.addAll(ConditionalFormatHelper.createFlashingFormat("pricerNavLast"));
            }
            if (m_viewDef.getColumns().get("limitPrice") != null) {
                formats.addAll(ConditionalFormatHelper.createFlashingFormat("limitPrice"));
            }
            if (m_viewDef.getColumns().get("realizedPnL") != null) {
                formats.addAll(ConditionalFormatHelper.createFlashingFormat(
                  "realizedPnL",
                  "unrealizedPnL",
                  "totalPnL",
                  "delta",
                  "costBasis"));
            }
            // formats.add(
            // ConditionalFormatHelper
            // .createCellBackgroundFormat("(== assetClass 'EQU')", "bloomberg", "bg-unknown"));
            t.viewConfigDelta(ViewConfigBuilder.newBuilder(u).formats(formats).get());
            t.setViewConfigListener(this);
        }
    }

    public static GridFormat createCountryFormat(String... columnNames) {
        GridFormatBuilder fb = GridFormatBuilder.newBuilder();
        fb.formatType("raw");
        List<ConditionalFormat> cfs = new ArrayList<>();
        cfs.add(ConditionalFormatBuilder.newBuilder()
          .columnNames(Arrays.asList(columnNames))
          .formatters(Arrays.asList("<span class=\"flag\"><img src='png/", ".PNG'>&nbsp;&nbsp;", "</span>"))
          .get());
        fb.conditionalFormats(cfs);
        return fb.get();
    }

    public static List<GridFormat> createFlashingFormat(String... columnNames) {
        List<GridFormat> formats = new ArrayList<>();

        for (String columnName : columnNames) {
            GridFormatBuilder cellFlash = GridFormatBuilder.newBuilder();
            List<ConditionalFormat> cfs = new ArrayList<>();
            cfs.add(ConditionalFormatBuilder.newBuilder()
              .condition("(> pctChange." + columnName + " 0)")
              .columnNames(Arrays.asList(columnName))
              .formatters(Arrays.asList("flashuptick"))
              .get());
            cfs.add(ConditionalFormatBuilder.newBuilder()
              .condition("(< pctChange." + columnName + " 0)")
              .columnNames(Arrays.asList(columnName))
              .formatters(Arrays.asList("flashdowntick"))
              .get());

            cellFlash.conditionalFormats(cfs);
            formats.add(cellFlash.get());
        }

        GridFormatBuilder cellFlash = GridFormatBuilder.newBuilder();
        List<ConditionalFormat> cfs = new ArrayList<>();
        cfs.add(ConditionalFormatBuilder.newBuilder()
          .columnNames(Arrays.asList(columnNames))
          .formatters(Arrays.asList("tick"))
          .get());
        cellFlash.conditionalFormats(cfs);
        formats.add(cellFlash.get());

        return formats;
    }

    // available colors: red, yellow, blue, green
    public static GridFormat createRowBackgroundFormat(final String condition, final String... colors) {
        final GridFormatBuilder builder = GridFormatBuilder.newBuilder();
        final List<String> cssClasses =
          Arrays.asList(colors).stream().map(c -> "row-bg-" + c).collect(Collectors.toList());
        final List<ConditionalFormat> cfs = new ArrayList<>();
        cfs.add(ConditionalFormatBuilder.newBuilder().condition(condition).formatters(cssClasses).get());
        builder.conditionalFormats(cfs);
        return builder.get();
    }

    public static GridFormat createBuySellFormat(final String... columnNames) {
        final GridFormatBuilder builder = GridFormatBuilder.newBuilder();
        for (final String columnName : columnNames) {
            final List<ConditionalFormat> cfs = new ArrayList<>();
            cfs.add(ConditionalFormatBuilder.newBuilder()
              .condition("(== " + columnName + " 'Buy')")
              .columnNames(Arrays.asList(columnName))
              .formatters(Arrays.asList("bg-buy"))
              .get());
            cfs.add(ConditionalFormatBuilder.newBuilder()
              .condition("(== " + columnName + " 'Sell')")
              .columnNames(Arrays.asList(columnName))
              .formatters(Arrays.asList("bg-sell"))
              .get());
            cfs.add(ConditionalFormatBuilder.newBuilder()
              .condition("(== " + columnName + " 'BuySell')")
              .columnNames(Arrays.asList(columnName))
              .formatters(Arrays.asList("bg-buysell"))
              .get());

            builder.conditionalFormats(cfs);
        }
        return builder.get();
    }

    public static GridFormat createCellBackgroundFormat(
      final String condition, final String columnName, final String color) {
        final GridFormatBuilder builder = GridFormatBuilder.newBuilder();
        final List<ConditionalFormat> cfs = new ArrayList<>();
        cfs.add(ConditionalFormatBuilder.newBuilder()
          .condition(condition)
          .columnNames(Arrays.asList(columnName))
          .formatters(Arrays.asList(color))
          .get());
        return builder.conditionalFormats(cfs).get();
    }

    // add a tooltip for the long text
    public static GridFormat createLongTextFormat(final String... columnNames) {
        final GridFormatBuilder builder = GridFormatBuilder.newBuilder();
        builder.formatType("raw");
        final List<ConditionalFormat> cfs = new ArrayList<>();
        cfs.add(ConditionalFormatBuilder.newBuilder()
          .columnNames(Arrays.asList(columnNames))
          .formatters(Arrays.asList("<span title=\"", "\">", "</span>"))
          .get());
        builder.conditionalFormats(cfs);
        return builder.get();
    }

    public static GridFormat createBooleanFormat(final String... columnNames) {
        final GridFormatBuilder builder = GridFormatBuilder.newBuilder();
        builder.formatType("raw");
        final List<ConditionalFormat> cfs = new ArrayList<>();
        cfs.add(ConditionalFormatBuilder.newBuilder().columnNames(Arrays.asList(columnNames)).formatters(Arrays.asList(
          "<span style=\"display:inline-block;height:16px;width:16px;background-size:16px 16px;"
            + "background-repeat:no-repeat;background-image:url('/velox-core/png/",
          ".png')\"></span>")).get());
        builder.conditionalFormats(cfs);
        return builder.get();
    }

    // available colors: red, yellow, blue, green
    public static GridFormat createNormalProgressFormat(final String color, final String... columnNames) {
        final GridFormatBuilder builder = GridFormatBuilder.newBuilder();
        builder.formatType("raw");
        final List<Integer> darkColor = ConditionalFormatHelper.darkColor(color);
        final String backgroundColor = ConditionalFormatHelper.backgroundColor(darkColor, 1.0);
        final List<ConditionalFormat> cfs = new ArrayList<>();
        cfs.add(ConditionalFormatBuilder.newBuilder()
          .columnNames(Arrays.asList(columnNames))
          .formatters(ConditionalFormatHelper.progressFormatter(backgroundColor))
          .get());
        builder.conditionalFormats(cfs);
        return builder.get();
    }

    public static GridFormat createLeveledProgressFormat(final String color, final String columnName) {
        final GridFormatBuilder builder = GridFormatBuilder.newBuilder();
        builder.formatType("raw");
        final List<Integer> darkColor = ConditionalFormatHelper.darkColor(color);
        final List<ConditionalFormat> cfs = new ArrayList<>();
        final int LEVELS = 10;
        for (int i = 0; i <= LEVELS; ++i) {
            final double start = 1.0 * i / LEVELS;
            final double interval = 1.0 / LEVELS;
            final String backgroundColor = ConditionalFormatHelper.backgroundColor(darkColor, start);
            cfs.add(ConditionalFormatBuilder.newBuilder()
              .condition(ConditionalFormatHelper.rangeCondition(columnName, start, start + interval))
              .columnNames(Arrays.asList(columnName))
              .formatters(ConditionalFormatHelper.progressFormatter(backgroundColor))
              .get());
        }
        builder.conditionalFormats(cfs);
        return builder.get();
    }

    public static GridFormat createYellowToGreenProgressFormat(final String columnName) {
        final GridFormatBuilder builder = GridFormatBuilder.newBuilder();
        builder.formatType("raw");
        final List<ConditionalFormat> cfs = new ArrayList<>();
        final int LEVELS = 10;
        for (int i = 0; i <= LEVELS; ++i) {
            final double start = 1.0 * i / LEVELS;
            final double interval = 1.0 / LEVELS;
            final int half = LEVELS / 2;
            final List<Integer> darkColor =
              i < half ? ConditionalFormatHelper.PROGRESS_YELLOW : ConditionalFormatHelper.PROGRESS_GREEN;
            final String backgroundColor = i < half ?
              ConditionalFormatHelper.reverseBackgroundColor(darkColor, interval * 2) :
              ConditionalFormatHelper.backgroundColor(darkColor, (i - half) * interval * 2);
            cfs.add(ConditionalFormatBuilder.newBuilder()
              .condition(ConditionalFormatHelper.rangeCondition(columnName, start, start + interval))
              .columnNames(Arrays.asList(columnName))
              .formatters(ConditionalFormatHelper.progressFormatter(backgroundColor))
              .get());
        }
        builder.conditionalFormats(cfs);
        return builder.get();
    }

    private static String rangeCondition(final String columnName, final double startRange, final double endRange) {
        return String.format("(and (>= %s %f) (< %s %f))", columnName, startRange, columnName, endRange);
    }

    private static List<String> progressFormatter(final String backgroundColor) {
        return Arrays.asList(
          "<span class='progressbar' style='background-image: linear-gradient(to right, " + backgroundColor + " ",
          ", transparent 0);'>",
          "</span>");
    }

    private static String backgroundColor(final List<Integer> darkColor, final double value) {
        final List<Integer> rgb = darkColor.stream().map(dark -> {
            final double light = dark + (255 - dark) * 3 / 4;
            return (int) Math.floor(light - (light - dark) * value);
        }).collect(Collectors.toList());
        return String.format("rgb(%d, %d, %d)", rgb.get(0), rgb.get(1), rgb.get(2));
    }

    private static String reverseBackgroundColor(final List<Integer> darkColor, final double value) {
        final List<Integer> rgb = darkColor.stream().map(dark -> {
            final double light = dark + (255 - dark) * 3 / 4;
            return (int) Math.floor(dark + (light - dark) * value);
        }).collect(Collectors.toList());
        return String.format("rgb(%d, %d, %d)", rgb.get(0), rgb.get(1), rgb.get(2));
    }

    private static List<Integer> darkColor(final String color) {
        switch (color) {
            case "red":
                return ConditionalFormatHelper.PROGRESS_RED;
            case "yellow":
                return ConditionalFormatHelper.PROGRESS_YELLOW;
            case "green":
                return ConditionalFormatHelper.PROGRESS_GREEN;
            case "pink":
                return ConditionalFormatHelper.PROGRESS_PINK;
            case "grey":
                return ConditionalFormatHelper.PROGRESS_GREY;
            case "purple":
                return ConditionalFormatHelper.PROGRESS_PURPLE;
            default:
                return ConditionalFormatHelper.PROGRESS_BLUE;
        }
    }

    // RGB values from gentelella admin theme
    private static final List<Integer> PROGRESS_RED = Arrays.asList(217, 83, 79);
    private static final List<Integer> PROGRESS_YELLOW = Arrays.asList(240, 173, 78);
    private static final List<Integer> PROGRESS_BLUE = Arrays.asList(52, 152, 219);
    private static final List<Integer> PROGRESS_GREEN = Arrays.asList(38, 185, 154);
    private static final List<Integer> PROGRESS_PINK = Arrays.asList(244, 66, 226);
    private static final List<Integer> PROGRESS_PURPLE = Arrays.asList(109, 34, 170);
    private static final List<Integer> PROGRESS_GREY = Arrays.asList(66, 62, 68);

}

