package uk.co.ghco.util;

import com.aralis.gooey.api.ScreenRelationshipType;
import com.aralis.gooey.api.WorkspaceHintsPOJOImplBuilder;
import com.aralis.vm.ClientNotifier;
import com.aralis.vm.Screen;

public class ModalHelper {
    static public void popup(final Screen modalScreen, final Screen parentScreen, final ClientNotifier clientNotifier) {
        modalScreen.onRequestClose(t -> {
            return true;
        });

        clientNotifier.created(
          modalScreen,
          new WorkspaceHintsPOJOImplBuilder()
            .relationshipType(ScreenRelationshipType.MODAL)
            .id(parentScreen.controlId())
            .get());
    }

}
