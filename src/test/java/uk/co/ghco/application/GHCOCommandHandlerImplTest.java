package uk.co.ghco.application;

import org.junit.Before;
import org.junit.Test;
import uk.co.ghco.api.*;
import uk.co.ghco.api.ghcoengine.server.GHCOEngineBlackBox;
import uk.co.ghco.api.ghcoengine.server.GHCOEngineContext;
import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static uk.co.ghco.application.GHCOMatchers.sameAs;

public class GHCOCommandHandlerImplTest {
    private static final String PRINCIPAL = "PRINCIPAL";
    private static final String CLIENT = "CLIENT";
    private static final String ERROR = "ERROR";
    private static final String CONTROL = "CONTROL";
    // the system under testing. RMSBlackBox is the server component running the command
    // handler and capturing the results.
    private GHCOEngineBlackBox m_sut;

    @Before
    public void setup() {
        // setup the RMSBlackBox so that it can be exercised without a running server. Every test will be using
        // a new instance of RMSBlackBox so there is no leftover states from the previous test
        m_sut = GHCOEngineSEDescriptor.createTestStateEngine("t",
          "t",
          GHCOCommandHandlerImpl.hashIndicesInfo(),
          new GHCOCommandHandlerImpl());
    }

    @Test
    public void test_longPosition_create() {
        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("APPL")
          .tradeCcy("USD")
          .side(Side.Buy)
          .externalId("TRADEID100")
          .tradeQty(10000)
          .tradePrice(120)
          .get());

        final Position position = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("APPL")
          .currency("USD")
          .position(10000)
          .costBasis(120)
          .boughtQty(10000)
          .boughtPrice(120)
          .lastTradeId("TRADEID100")
          .lastTradeVersion(0)
          .version(0)
          .get();

        final GHCOEngineContext context = m_sut.readOnlyContext();
        assertThat(context.getPositionByPrimary(PositionExtractor.Primary.get(position)), sameAs(position));
    }

    @Test
    public void test_longPosition_increase() {
        // setup using a drop-copy command
        Position position = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(10000)
          .boughtPrice(120)
          .position(10000)
          .costBasis(121)
          .get();

        m_sut.handleCommand(position);

        // exercise the system
        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Buy)
          .externalId("TRADEID100")
          .tradeQty(10000)
          .tradePrice(120)
          .get());

        // assertions
        final Position expect = PositionBuilder.newBuilder(position)
          .position(20000)
          .costBasis(120.5)
          .boughtQty(20000)
          .boughtPrice(120)
          .lastTradeId("TRADEID100")
          .lastTradeVersion(0)
          .version(1)
          .get();
        final GHCOEngineContext context = m_sut.readOnlyContext();
        assertThat(context.getPositionByPrimary(PositionExtractor.Primary.get(position)), sameAs(expect));
    }

    @Test
    public void test_longPosition_unwind() {
        // setup using a drop-copy command
        Position position = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(100)
          .boughtPrice(12)
          .position(100)
          .costBasis(12)
          .get();

        m_sut.handleCommand(position);

        // exercise the system
        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Sell)
          .externalId("TRADEID100")
          .tradeQty(40)
          .tradePrice(11)
          .get());

        // unwinding position only affects PnL, not the cost basis
        final Position expect = PositionBuilder.newBuilder(position)
          .position(60)
          .costBasis(12)
          .soldQty(40)
          .soldPrice(11)
          .lastTradeId("TRADEID100")
          .lastTradeVersion(0)
          .lastTradePnL(-40)
          .realizedPnL(-40)
          .version(1)
          .get();
        final GHCOEngineContext context = m_sut.readOnlyContext();
        assertThat(context.getPositionByPrimary(PositionExtractor.Primary.get(position)), sameAs(expect));
    }

    @Test
    public void test_longPosition_unwind_toZero() {
        // setup using a drop-copy command
        Position position = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(100)
          .boughtPrice(12)
          .position(100)
          .costBasis(12)
          .get();

        m_sut.handleCommand(position);

        // exercise the system
        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Sell)
          .externalId("TRADEID100")
          .tradeQty(100)
          .tradePrice(13)
          .get());

        final Position expect = PositionBuilder.newBuilder(position)
          .position(0)
          .costBasis(0)
          .soldQty(100)
          .soldPrice(13)
          .lastTradeId("TRADEID100")
          .lastTradeVersion(0)
          .lastTradePnL(100)
          .realizedPnL(100)
          .version(1)
          .get();
        final GHCOEngineContext context = m_sut.readOnlyContext();
        assertThat(context.getPositionByPrimary(PositionExtractor.Primary.get(position)), sameAs(expect));
    }

    @Test
    public void test_longPosition_unwind_toShort() {
        // setup using a drop-copy command
        Position position = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(100)
          .boughtPrice(12)
          .position(100)
          .costBasis(12)
          .get();

        m_sut.handleCommand(position);

        // exercise the system
        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Sell)
          .externalId("TRADEID100")
          .tradeQty(160)
          .tradePrice(13)
          .get());

        final Position expect = PositionBuilder.newBuilder(position)
          .position(-60)
          .costBasis(13)
          .soldQty(160)
          .soldPrice(13)
          .lastTradeId("TRADEID100")
          .lastTradeVersion(0)
          .lastTradePnL(100)
          .realizedPnL(100)
          .version(1)
          .get();
        final GHCOEngineContext context = m_sut.readOnlyContext();
        assertThat(context.getPositionByPrimary(PositionExtractor.Primary.get(position)), sameAs(expect));
    }

    @Test
    public void test_longPosition_unwind_toZero_thenIncrease() {
        // setup using a drop-copy command
        Position position = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .position(100)
          .costBasis(12)
          .get();

        m_sut.handleCommand(position);

        // exercise the system
        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Sell)
          .externalId("TRADEID100")
          .tradeQty(100)
          .tradePrice(13)
          .get());

        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Buy)
          .externalId("TRADEID101")
          .tradeQty(70)
          .tradePrice(11)
          .get());

        final Position expect = PositionBuilder.newBuilder(position)
          .position(70)
          .costBasis(11)
          .realizedPnL(100)
          .soldQty(100)
          .soldPrice(13)
          .boughtQty(70)
          .boughtPrice(11)
          .lastTradeId("TRADEID101")
          .lastTradeVersion(0)
          .lastTradePnL(0)
          .version(2)
          .get();
        final GHCOEngineContext context = m_sut.readOnlyContext();
        assertThat(context.getPositionByPrimary(PositionExtractor.Primary.get(position)), sameAs(expect));
    }

    @Test
    public void test_longPosition_boughtPrice_average() {
        Position position = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .position(100)
          .costBasis(51)
          .boughtQty(100)
          .boughtPrice(60)
          .get();

        m_sut.handleCommand(position);

        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Buy)
          .externalId("TRADEID100")
          .tradeQty(200)
          .tradePrice(54)
          .get());

        final Position expect = PositionBuilder.newBuilder(position)
          .position(300)
          .costBasis(53)
          .boughtQty(300)
          .boughtPrice(56)
          .lastTradeId("TRADEID100")
          .lastTradeVersion(0)
          .version(1)
          .get();
        final GHCOEngineContext context = m_sut.readOnlyContext();
        assertThat(context.getPositionByPrimary(PositionExtractor.Primary.get(position)), sameAs(expect));
    }

    @Test
    public void test_longPosition_soldPrice_average() {
        Position position = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .position(100)
          .costBasis(51)
          .soldQty(100)
          .soldPrice(60)
          .get();

        m_sut.handleCommand(position);

        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Sell)
          .externalId("TRADEID100")
          .tradeQty(200)
          .tradePrice(54)
          .get());

        final Position expect = PositionBuilder.newBuilder(position)
          .position(-100)
          .costBasis(54)
          .realizedPnL(300)
          .soldQty(300)
          .soldPrice(56)
          .lastTradeId("TRADEID100")
          .lastTradeVersion(0)
          .lastTradePnL(300)
          .version(1)
          .get();
        final GHCOEngineContext context = m_sut.readOnlyContext();
        assertThat(context.getPositionByPrimary(PositionExtractor.Primary.get(position)), sameAs(expect));
    }

    @Test
    public void test_shortPosition_create() {
        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("APPL")
          .tradeCcy("USD")
          .side(Side.Sell)
          .externalId("TRADEID100")
          .tradeQty(10000)
          .tradePrice(120)
          .get());

        final Position position = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("APPL")
          .currency("USD")
          .position(-10000)
          .costBasis(120)
          .soldQty(10000)
          .soldPrice(120)
          .lastTradeId("TRADEID100")
          .lastTradeVersion(0)
          .version(0)
          .get();

        final GHCOEngineContext context = m_sut.readOnlyContext();
        assertThat(context.getPositionByPrimary(PositionExtractor.Primary.get(position)), sameAs(position));
    }

    @Test
    public void test_shortPosition_increase() {
        Position position = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(10000)
          .boughtPrice(120)
          .position(10000)
          .costBasis(121)
          .lastTradeId("TRADEID99")
          .get();

        m_sut.handleCommand(position);

        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Sell)
          .externalId("TRADEID100")
          .tradeQty(20000)
          .tradePrice(125)
          .get());

        final Position expect = PositionBuilder.newBuilder(position)
          .position(-10000)
          .costBasis(125)
          .boughtQty(10000)
          .boughtPrice(120)
          .soldQty(20000)
          .soldPrice(125)
          .lastTradePnL(40000)
          .realizedPnL(40000)
          .lastTradeId("TRADEID100")
          .lastTradeVersion(0)
          .version(1)
          .get();
        final GHCOEngineContext context = m_sut.readOnlyContext();
        assertThat(context.getPositionByPrimary(PositionExtractor.Primary.get(position)), sameAs(expect));
    }

    @Test
    public void test_shortPosition_unwind() {
        Position position = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .position(-100)
          .costBasis(57)
          .lastTradeId("TRADEID99")
          .get();

        m_sut.handleCommand(position);

        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Buy)
          .externalId("TRADEID100")
          .tradeQty(40)
          .tradePrice(59)
          .get());

        final Position expect = PositionBuilder.newBuilder(position)
          .position(-60)
          .costBasis(57)
          .boughtQty(40)
          .boughtPrice(59)
          .lastTradePnL(-80)
          .realizedPnL(-80)
          .lastTradeId("TRADEID100")
          .lastTradeVersion(0)
          .version(1)
          .get();
        final GHCOEngineContext context = m_sut.readOnlyContext();
        assertThat(context.getPositionByPrimary(PositionExtractor.Primary.get(position)), sameAs(expect));
    }

    @Test
    public void test_shortPosition_unwind_toZero() {
        Position position = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .position(-100)
          .costBasis(57)
          .lastTradeId("TRADEID99")
          .get();

        m_sut.handleCommand(position);

        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Buy)
          .externalId("TRADEID100")
          .tradeQty(100)
          .tradePrice(53)
          .get());

        final Position expect = PositionBuilder.newBuilder(position)
          .position(0)
          .costBasis(0)
          .boughtQty(100)
          .boughtPrice(53)
          .lastTradePnL(400)
          .realizedPnL(400)
          .lastTradeId("TRADEID100")
          .lastTradeVersion(0)
          .version(1)
          .get();
        final GHCOEngineContext context = m_sut.readOnlyContext();
        assertThat(context.getPositionByPrimary(PositionExtractor.Primary.get(position)), sameAs(expect));
    }

    @Test
    public void test_shortPosition_unwind_toLong() {
        Position position = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .position(-100)
          .costBasis(57)
          .lastTradeId("TRADEID99")
          .get();

        m_sut.handleCommand(position);

        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Buy)
          .externalId("TRADEID100")
          .tradeQty(160)
          .tradePrice(53)
          .get());

        final Position expect = PositionBuilder.newBuilder(position)
          .position(60)
          .costBasis(53)
          .boughtQty(160)
          .boughtPrice(53)
          .lastTradePnL(400)
          .realizedPnL(400)
          .lastTradeId("TRADEID100")
          .lastTradeVersion(0)
          .version(1)
          .get();
        final GHCOEngineContext context = m_sut.readOnlyContext();
        assertThat(context.getPositionByPrimary(PositionExtractor.Primary.get(position)), sameAs(expect));
    }

    @Test
    public void test_costBasis() {
        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Buy)
          .externalId("TRADEID100")
          .tradeQty(3)
          .tradePrice(5)
          .get());
        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Sell)
          .externalId("TRADEID101")
          .tradeQty(1)
          .tradePrice(4)
          .get());
        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Buy)
          .externalId("TRADEID102")
          .tradeQty(2)
          .tradePrice(6)
          .get());

        final Object key = PositionExtractor.Primary.keyBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .portfolioName(null)
          .get();

        Position position = m_sut.readOnlyContext().getPositionByPrimary(key);
        assertThat(position.costBasis(), equalTo(5.5));
    }

    @Test
    public void test_costBasis_reserveSide() {
        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Buy)
          .externalId("TRADEID100")
          .tradeQty(3)
          .tradePrice(5)
          .get());
        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Sell)
          .externalId("TRADEID101")
          .tradeQty(1)
          .tradePrice(4)
          .get());
        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Buy)
          .externalId("TRADEID102")
          .tradeQty(2)
          .tradePrice(6)
          .get());

        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .side(Side.Sell)
          .externalId("TRADEID103")
          .tradeQty(10)
          .tradePrice(3.5)
          .get());
        final Object key = PositionExtractor.Primary.keyBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .portfolioName(null)
          .get();

        Position position = m_sut.readOnlyContext().getPositionByPrimary(key);
        assertThat(position.costBasis(), equalTo(3.5));
    }

    @Test
    public void test_cancel_flippingTrade_fromMiddle() {
        final GHCOEngineContext context = m_sut.readOnlyContext();

        final CreateTradeBuilder createTrade = CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD");

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Buy).tradeQty(2).tradePrice(5.5).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-2").side(Side.Sell).tradeQty(3).tradePrice(7).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-3").side(Side.Buy).tradeQty(2).tradePrice(4.5).get());

        m_sut.handleCommand(CancelTradeBuilder.newBuilder().tradeId("TRADE-2").get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(4)
          .boughtPrice(5)
          .soldQty(0)
          .soldPrice(0)
          .position(4)
          .costBasis(5)
          .realizedPnL(0)
          .lastTradeId("TRADE-3")
          .lastTradeVersion(0)
          .lastTradePnL(0)
          .version(3)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_cancel_flippingTrade_fromEnd() {
        final GHCOEngineContext context = m_sut.readOnlyContext();

        final CreateTradeBuilder createTrade = CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD");

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Buy).tradeQty(2).tradePrice(5).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-2").side(Side.Buy).tradeQty(3).tradePrice(7).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-3").side(Side.Sell).tradeQty(7).tradePrice(6).get());

        m_sut.handleCommand(CancelTradeBuilder.newBuilder().tradeId("TRADE-3").get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(5)
          .boughtPrice(6.2)
          .soldQty(0)
          .soldPrice(0)
          .position(5)
          .costBasis(6.2)
          .realizedPnL(0)
          .lastTradeId("TRADE-3")
          .lastTradeVersion(1)
          .lastTradePnL(0)
          .version(3)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_cancel_acquireTrade_fromBeginning() {
        final GHCOEngineContext context = m_sut.readOnlyContext();

        final CreateTradeBuilder createTrade = CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD");

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Buy).tradeQty(1).tradePrice(3).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-2").side(Side.Buy).tradeQty(2).tradePrice(6).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-3").side(Side.Buy).tradeQty(3).tradePrice(7).get());

        m_sut.handleCommand(CancelTradeBuilder.newBuilder().tradeId("TRADE-1").get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(5)
          .boughtPrice(6.6)
          .soldQty(0)
          .soldPrice(0)
          .position(5)
          .costBasis(6.6)
          .realizedPnL(0)
          .lastTradeId("TRADE-3")
          .lastTradeVersion(0)
          .lastTradePnL(0)
          .version(3)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_cancel_acquireTrade_fromMiddle() {
        final GHCOEngineContext context = m_sut.readOnlyContext();

        final CreateTradeBuilder createTrade = CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD");

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Buy).tradeQty(1).tradePrice(3).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-2").side(Side.Buy).tradeQty(2).tradePrice(6).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-3").side(Side.Buy).tradeQty(3).tradePrice(7).get());

        m_sut.handleCommand(CancelTradeBuilder.newBuilder().tradeId("TRADE-2").get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(4)
          .boughtPrice(6)
          .soldQty(0)
          .soldPrice(0)
          .position(4)
          .costBasis(6)
          .realizedPnL(0)
          .lastTradeId("TRADE-3")
          .lastTradeVersion(0)
          .lastTradePnL(0)
          .version(3)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_cancel_acquireTrade_fromEnd() {
        final GHCOEngineContext context = m_sut.readOnlyContext();

        final CreateTradeBuilder createTrade = CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD");

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Buy).tradeQty(1).tradePrice(3).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-2").side(Side.Buy).tradeQty(2).tradePrice(6).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-3").side(Side.Buy).tradeQty(3).tradePrice(7).get());

        m_sut.handleCommand(CancelTradeBuilder.newBuilder().tradeId("TRADE-3").get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(3)
          .boughtPrice(5)
          .soldQty(0)
          .soldPrice(0)
          .position(3)
          .costBasis(5)
          .realizedPnL(0)
          .lastTradeId("TRADE-3")
          .lastTradeVersion(1)
          .lastTradePnL(0)
          .version(3)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_correct_acquireTrade_fromBeginning() {
        final GHCOEngineContext context = m_sut.readOnlyContext();

        final CreateTradeBuilder createTrade = CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD");

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Buy).tradeQty(2).tradePrice(5).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-2").side(Side.Buy).tradeQty(3).tradePrice(7).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-3").side(Side.Sell).tradeQty(7).tradePrice(6).get());

        m_sut.handleCommand(CorrectTradeBuilder.newBuilder().tradeId("TRADE-1").tradeQty(5).tradePrice(5).get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(8)
          .boughtPrice(5.75)
          .soldQty(7)
          .soldPrice(6)
          .position(1)
          .costBasis(5.75)
          .realizedPnL(1.75)
          .lastTradeId("TRADE-3")
          .lastTradeVersion(0)
          .lastTradePnL(1.75)
          .version(3)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_correct_acquireTrade_fromMiddle() {
        final GHCOEngineContext context = m_sut.readOnlyContext();

        final CreateTradeBuilder createTrade = CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD");

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Buy).tradeQty(2).tradePrice(5).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-2").side(Side.Buy).tradeQty(3).tradePrice(7).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-3").side(Side.Sell).tradeQty(7).tradePrice(6).get());

        m_sut.handleCommand(CorrectTradeBuilder.newBuilder().tradeId("TRADE-2").tradeQty(1).tradePrice(8).get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(3)
          .boughtPrice(6)
          .soldQty(7)
          .soldPrice(6)
          .position(-4)
          .costBasis(6)
          .realizedPnL(0)
          .lastTradeId("TRADE-3")
          .lastTradeVersion(0)
          .lastTradePnL(0)
          .version(3)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_correct_acquireTrade_fromEnd() {
        final GHCOEngineContext context = m_sut.readOnlyContext();

        final CreateTradeBuilder createTrade = CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD");

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Buy).tradeQty(2).tradePrice(5).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-2").side(Side.Buy).tradeQty(3).tradePrice(7).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-3").side(Side.Buy).tradeQty(7).tradePrice(6).get());

        m_sut.handleCommand(CorrectTradeBuilder.newBuilder().tradeId("TRADE-3").tradeQty(3).tradePrice(8).get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(8)
          .boughtPrice(6.875)
          .soldQty(0)
          .soldPrice(0)
          .position(8)
          .costBasis(6.875)
          .realizedPnL(0)
          .lastTradeId("TRADE-3")
          .lastTradeVersion(1)
          .lastTradePnL(0)
          .version(3)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_correct_unwindingTrade_fromMiddle() {
        final GHCOEngineContext context = m_sut.readOnlyContext();

        final CreateTradeBuilder createTrade = CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD");

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Buy).tradeQty(10).tradePrice(5).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-2").side(Side.Sell).tradeQty(4).tradePrice(7).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-3").side(Side.Buy).tradeQty(5).tradePrice(6.5).get());

        m_sut.handleCommand(CorrectTradeBuilder.newBuilder().tradeId("TRADE-2").tradeQty(3).tradePrice(7).get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(15)
          .boughtPrice(5.5)
          .soldQty(3)
          .soldPrice(7)
          .position(12)
          .costBasis(5.625)
          .realizedPnL(6)
          .lastTradeId("TRADE-3")
          .lastTradeVersion(0)
          .lastTradePnL(0)
          .version(3)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_correct_unwindingTrade_fromEnd() {
        final GHCOEngineContext context = m_sut.readOnlyContext();

        final CreateTradeBuilder createTrade = CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD");

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Sell).tradeQty(9).tradePrice(5).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-2").side(Side.Sell).tradeQty(4).tradePrice(8.25).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-3").side(Side.Buy).tradeQty(5).tradePrice(6.5).get());

        m_sut.handleCommand(CorrectTradeBuilder.newBuilder().tradeId("TRADE-3").tradeQty(3).tradePrice(7).get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(3)
          .boughtPrice(7)
          .soldQty(13)
          .soldPrice(6)
          .position(-10)
          .costBasis(6)
          .realizedPnL(-3)
          .lastTradeId("TRADE-3")
          .lastTradeVersion(1)
          .lastTradePnL(-3)
          .version(3)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_correct_flippingTrade_fromMiddle() {
        final GHCOEngineContext context = m_sut.readOnlyContext();

        final CreateTradeBuilder createTrade = CreateTradeBuilder.newBuilder()
          .tradeDate(LocalDate.of(2019, 1, 1))
          .accountId(PRINCIPAL)
          .tradeListingId("IBM")
          .tradeCcy("USD");

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Buy).tradeQty(9).tradePrice(5).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-2").side(Side.Sell).tradeQty(10).tradePrice(6).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-3").side(Side.Buy).tradeQty(6).tradePrice(5.5).get());

        m_sut.handleCommand(CorrectTradeBuilder.newBuilder().tradeId("TRADE-2").tradeQty(7).tradePrice(7).get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(15)
          .boughtPrice(5.2)
          .soldQty(7)
          .soldPrice(7)
          .position(8)
          .costBasis(5.375)
          .realizedPnL(14)
          .lastTradeId("TRADE-3")
          .lastTradeVersion(0)
          .lastTradePnL(0)
          .version(3)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_correct_with_sod() {
        final GHCOEngineContext context = m_sut.readOnlyContext();
        final LocalDate date = LocalDate.of(2019, 1, 1);
        m_sut.handleCommand(CreatePositionSnapBuilder.newBuilder()
          .snapDate(date)
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .quantity(4)
          .costBasis(4)
          .get());

        final CreateTradeBuilder createTrade =
          CreateTradeBuilder.newBuilder().tradeDate(date).accountId(PRINCIPAL).tradeListingId("IBM").tradeCcy("USD");

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Buy).tradeQty(5).tradePrice(5.8).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-2").side(Side.Sell).tradeQty(10).tradePrice(6).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-3").side(Side.Buy).tradeQty(5).tradePrice(6.4).get());

        m_sut.handleCommand(CorrectTradeBuilder.newBuilder().tradeId("TRADE-2").tradeQty(7).tradePrice(7).get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(10)
          .boughtPrice(6.1)
          .soldQty(7)
          .soldPrice(7)
          .position(7)
          .costBasis(6)
          .realizedPnL(14)
          .lastTradeId("TRADE-3")
          .lastTradeVersion(0)
          .lastTradePnL(0)
          .version(4)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_complicate_scenario() {
        final GHCOEngineContext context = m_sut.readOnlyContext();
        final LocalDate date = LocalDate.of(2019, 1, 1);
        m_sut.handleCommand(CreatePositionSnapBuilder.newBuilder()
          .snapDate(date)
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .quantity(5)
          .costBasis(4)
          .get());

        final CreateTradeBuilder createTrade =
          CreateTradeBuilder.newBuilder().tradeDate(date).accountId(PRINCIPAL).tradeListingId("IBM").tradeCcy("USD");

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Buy).tradeQty(5).tradePrice(6.3).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-2").side(Side.Sell).tradeQty(15).tradePrice(7).get());
        m_sut.handleCommand(CorrectTradeBuilder.newBuilder().tradeId("TRADE-2").tradeQty(6).tradePrice(7).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-3").side(Side.Buy).tradeQty(5).tradePrice(10.4).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-4").side(Side.Buy).tradeQty(12).tradePrice(6).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-5").side(Side.Sell).tradeQty(2).tradePrice(5).get());
        m_sut.handleCommand(CorrectTradeBuilder.newBuilder().tradeId("TRADE-4").tradeQty(1).tradePrice(3).get());
        m_sut.handleCommand(CancelTradeBuilder.newBuilder().tradeId("TRADE-4").get());
        m_sut.handleCommand(CorrectTradeBuilder.newBuilder().tradeId("TRADE-1").tradeQty(5).tradePrice(6).get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(10)
          .boughtPrice(8.2)
          .soldQty(8)
          .soldPrice(6.5)
          .position(7)
          .costBasis(8)
          .realizedPnL(6)
          .lastTradeId("TRADE-5")
          .lastTradeVersion(0)
          .lastTradePnL(-6)
          .version(9)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_versus_account() {
        final GHCOEngineContext context = m_sut.readOnlyContext();
        final LocalDate date = LocalDate.of(2019, 1, 1);
        m_sut.handleCommand(CreatePositionSnapBuilder.newBuilder()
          .snapDate(date)
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .quantity(5)
          .costBasis(4)
          .get());

        m_sut.handleCommand(CreateTradeBuilder.newBuilder()
          .tradeDate(date)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .externalId("TRADE-1")
          .accountId(CLIENT)
          .versusAccountId(PRINCIPAL)
          .side(Side.Buy)
          .tradeQty(6)
          .tradePrice(10)
          .get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(0)
          .boughtPrice(0)
          .soldQty(6)
          .soldPrice(10)
          .position(-1)
          .costBasis(10)
          .realizedPnL(30)
          .lastTradeId("TRADE-1")
          .lastTradeVersion(0)
          .lastTradePnL(30)
          .version(1)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_versus_account_correctTrade() {
        final GHCOEngineContext context = m_sut.readOnlyContext();
        final LocalDate date = LocalDate.of(2019, 1, 1);
        m_sut.handleCommand(CreatePositionSnapBuilder.newBuilder()
          .snapDate(date)
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .quantity(5)
          .costBasis(4)
          .get());

        CreateTradeBuilder createTrade = CreateTradeBuilder.newBuilder()
          .tradeDate(date)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .accountId(CLIENT)
          .versusAccountId(PRINCIPAL);

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Buy).tradeQty(6).tradePrice(10).get());
        m_sut.handleCommand(CorrectTradeBuilder.newBuilder().externalRefId("TRADE-1").tradeQty(6).tradePrice(9).get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(0)
          .boughtPrice(0)
          .soldQty(6)
          .soldPrice(9)
          .position(-1)
          .costBasis(9)
          .realizedPnL(25)
          .lastTradeId("TRADE-1")
          .lastTradeVersion(1)
          .lastTradePnL(25)
          .version(2)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }

    @Test
    public void test_versus_account_cancelTrade() {
        final GHCOEngineContext context = m_sut.readOnlyContext();
        final LocalDate date = LocalDate.of(2019, 1, 1);
        m_sut.handleCommand(CreatePositionSnapBuilder.newBuilder()
          .snapDate(date)
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .quantity(5)
          .costBasis(4)
          .get());

        CreateTradeBuilder createTrade = CreateTradeBuilder.newBuilder()
          .tradeDate(date)
          .tradeListingId("IBM")
          .tradeCcy("USD")
          .accountId(CLIENT)
          .versusAccountId(PRINCIPAL);

        m_sut.handleCommand(createTrade.externalId("TRADE-1").side(Side.Buy).tradeQty(6).tradePrice(10).get());
        m_sut.handleCommand(createTrade.externalId("TRADE-2").side(Side.Sell).tradeQty(1).tradePrice(16).get());
        m_sut.handleCommand(CancelTradeBuilder.newBuilder().externalRefId("TRADE-1").get());

        Position expect = PositionBuilder.newBuilder()
          .accountId(PRINCIPAL)
          .listingId("IBM")
          .currency("USD")
          .boughtQty(1)
          .boughtPrice(16)
          .soldQty(0)
          .soldPrice(0)
          .position(6)
          .costBasis(6)
          .realizedPnL(0)
          .lastTradeId("TRADE-2")
          .lastTradeVersion(0)
          .lastTradePnL(0)
          .version(3)
          .get();
        Position actual = context.getPositionByPrimary(PositionExtractor.Primary.get(expect));

        assertThat(actual, sameAs(expect));
    }
}

