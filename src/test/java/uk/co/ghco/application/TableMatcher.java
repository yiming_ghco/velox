package uk.co.ghco.application;

import com.aralis.df.extractor.Extractor;
import com.aralis.df.extractor.TableDescriptor;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

public class TableMatcher<T> extends TypeSafeMatcher<T> {
    private final T m_expect;
    private final Collection<Extractor<T, ?>> m_extractors;
    private Collection<Extractor<T, ?>> m_mismatch = new ArrayList<>();

    TableMatcher(final T expect, TableDescriptor<T, ?> descriptor) {
        this(expect, descriptor.getChainedExtractors().values());
    }

    TableMatcher(final T expect, Collection<Extractor<T, ?>> extractors) {
        this(expect, extractors, Collections.emptyList());
    }

    TableMatcher(final T expect, Collection<Extractor<T, ?>> include, Collection<Extractor<T, ?>> exclude) {
        m_expect = expect;
        m_extractors = new ArrayList<>(include);
        m_extractors.removeAll(exclude);
    }

    @Override
    protected boolean matchesSafely(final T t) {
        m_mismatch = m_extractors.stream().filter(it -> !it.equals(m_expect, t)).collect(Collectors.toList());
        return m_mismatch.isEmpty();
    }

    @Override
    public void describeTo(final Description description) {
        if (!m_mismatch.isEmpty()) {
            description.appendText(describe(m_expect, m_mismatch));
        } else {
            description.appendText(describe(m_expect, m_extractors));

        }
    }

    @Override
    public void describeMismatchSafely(final T t, Description description) {
        description.appendText(describe(t, m_mismatch));
    }

    private String describe(T t, Collection<Extractor<T, ?>> extractors) {
        return extractors.stream().map(it -> it.name() + " = " + it.get(t)).collect(joining(", "));
    }
}
