package uk.co.ghco.application;

import com.aralis.df.extractor.Extractor;
import com.aralis.df.extractor.TableExtractor;
import uk.co.ghco.api.*;
import java.util.Collection;
import java.util.stream.Collectors;

public class GHCOMatchers {
    public static TableMatcher<PositionSnap> sameAs(PositionSnap snap) {
        return new TableMatcher<>(
          snap,
          leavesWithoutStateIdAndMeta(PositionSnapExtractor.ChainedExtractorsMap.values()));
    }

    public static TableMatcher<Position> sameAs(Position position) {
        return new TableMatcher<>(
          position,
          leavesWithoutStateIdAndMeta(PositionExtractor.ChainedExtractorsMap.values()));
    }

    public static TableMatcher<PositionHistory> sameAs(PositionHistory history) {
        return new TableMatcher<>(history,
          leavesWithoutStateIdAndMeta(PositionHistoryExtractor.ChainedExtractorsMap.values()));
    }

    private static <T> Collection<Extractor<T, ?>> leavesWithoutStateIdAndMeta(Collection<Extractor<T, ?>> extractors) {
        return extractors.stream()
          .filter(it -> !(it instanceof TableExtractor))
          .filter(it -> !(it.name().contains("stateId") || it.name().contains("stateMeta")))
          .collect(Collectors.toList());
    }
}
